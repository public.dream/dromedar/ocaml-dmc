; SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
; SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: CC0-1.0

(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix gexp)
 (guix build-system dune)
 (guix build-system gnu)
 (guix build-system ocaml)
 ((guix licenses) #:prefix license:)
 (gnu packages autotools)
 (gnu packages compression)
 (gnu packages multiprecision)
 (gnu packages maths)
 (gnu packages ocaml)
 (gnu packages pkg-config)
 (gnu packages rdf)
 (gnu packages version-control))

(define-public ocaml-cbor
  (package
    (name "ocaml-cbor")
    (version "11261798db015a768d4759b33a529395e2ab5a30")
    (home-page "https://inqlab.net/git/ocaml-cbor.git")
    (source
     (origin (method git-fetch)
             (uri (git-reference
                   (url home-page)
                   (commit version)))
             (file-name (git-file-name name version))
             (sha256
              (base32 "12l3xz1jpnw87w0k506qh2hv8aiw0sq2fz5ri8rka1glizhb5wvn"))))
    (build-system dune-build-system)
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)
       ("qcheck" ,ocaml-qcheck)))
    (propagated-inputs
     `(("angstrom" ,ocaml-angstrom)
       ("zarith" ,ocaml-zarith)
       ("gmp" ,gmp)))
    (synopsis #f)
    (description #f)
    (license license:agpl3+)))

(define-public ocaml-rdf
  (package
    (name "ocaml-rdf")
    (version "82456a9db5b4b1fe6156707f0088182c3b671641")
    (home-page "https://codeberg.org/openEngiadina/ocaml-rdf.git")
    (source
     (origin
      (method git-fetch)
      (uri (git-reference
	    (url home-page)
            (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32 "1mm493sdjx81rfqiav3r9i0xjrv5dvsn34kv588vancf4dx0fa8r"))))
    (build-system dune-build-system)
    (arguments `(#:tests? #f))
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)
       ("qcheck" ,ocaml-qcheck)))
    (propagated-inputs
     `(("ocaml-uri" ,ocaml-uri)
       ("ocaml-yojson" ,ocaml-yojson)
       ("ocaml-cbor" ,ocaml-cbor)
       ("ocaml-angstrom" ,ocaml-angstrom)
       ("ocaml-ctypes" ,ocaml-ctypes)
       ("ocaml-xmlm" ,ocaml-xmlm)
       ("ocaml-uunf" ,ocaml-uunf)
       ("ocaml-uuidm" ,ocaml-uuidm)
       ("ocaml-z3" ,ocaml-z3)
       ("z3" ,z3)
       ("serd" ,serd)))
    (synopsis "RDF library for OCaml")
    (description #f)
    (license license:agpl3+)))

(define-public ocaml-monocypher
  (package
    (name "ocaml-monocypher")
    (version "d0029635c3e0f6fec3f28c6ee12e051ab3be56fe")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-monocypher")
                     (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "02s9c6i1ngzzfqgg689h5jxcsncylvnnl9pq7x56zlyxgkmbmg7r"))))
    (build-system dune-build-system)
    (propagated-inputs
     `(("ocaml-integers" ,ocaml-integers)
       ("ocaml-ctypes" ,ocaml-ctypes)))
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-monocypher")
    (synopsis "OCaml bindings to the Monocypher cryptographic library")
    (description #f)
    (license license:agpl3+)))

(define-public ocaml-base32
  (package
    (name "ocaml-base32")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-base32")
                     (commit "9bbe2a1fdb2ac3905b836aceb45336883baa2bf0")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0h6nsr3agkmf23zigz96jdb052305jpdgnf1rdnjjgj0bsb92bcb"))))
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     `(("qcheck" ,ocaml-qcheck)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-base32")
    (synopsis "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (description "This implements Base32 encoded as specified by RFC 4648 for OCaml")
    (license license:agpl3+)))

(define-public ocaml-eris
  (package
    (name "ocaml-eris")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.com/public.dream/dromedar/ocaml-eris")
                     (commit "cf9da42aa761e1db335d971d5c391bd2582453d5")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1rkpqkw9bsv6wali2vjz9c9s0q9lhmr2kz494q8jr31dzsrs0fwy"))))
    (build-system dune-build-system)
    (arguments '(#:tests? #f))
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)))
    (propagated-inputs
     `(("ocaml-monocypher" ,ocaml-monocypher)
       ("ocaml-base32" ,ocaml-base32)
       ("ocaml-yojson" ,ocaml-yojson)
       ("alcotest" ,ocaml-alcotest)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-eris")
    (synopsis "OCaml implementation of ERIS")
    (description "Ocaml implementation of ERIS, see homepage for a link to ERIS design document")
    (license license:agpl3+)))

(define-public ocaml-datalog
  (package
    (name "ocaml-datalog")
    (version "0.6")
    (home-page "https://github.com/c-cube/datalog")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "087v63kh13c342pnriaz4642hfllcxispclgz772nk312drar7h1"))))
    (build-system dune-build-system)
    (arguments '(#:tests? #f
                 #:phases
                 (modify-phases %standard-phases
                   (add-before 'build 'fix-deprecated
                     (lambda _
                       (substitute* "src/bottom_up_cli/datalog_cli.ml"
                         (("Pervasives.compare") "compare"))
                       #t)))))
    (propagated-inputs `(("ocaml-odoc" ,ocaml-odoc)
                         ("ocaml-stdlib" ,ocaml-stdlib-shims)))
    (native-inputs `(("ocaml-mdx" ,ocaml-mdx)))
    (synopsis
     "An in-memory datalog implementation for OCaml")
    (description "An in-memory datalog implementation for OCaml.

It features two main algorithm:
- bottom-up focuses on big sets of rules with small relations, with frequent
updates of the relations.  Therefore, it tries to achieve good behavior in
presence of incremental modifications of the relations.
- top-down resembles prolog (and allows nested subterms).  It handles stratified
negation and only explores the part of the search space that is relevant to a
given query. ")
    (license license:bsd-2)))

(define-public ocaml-containers
  (package
   (name "ocaml-containers")
   (version "3.4")
   (home-page "https://github.com/c-cube/ocaml-containers/")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url home-page)
           (commit (string-append "v" version))))
     (sha256
      (base32
       "0ixpy81p6rc3lq71djfndb2sg2hfj20j1jbzzrrmgqsysqdjsgzz"))))
   (build-system dune-build-system)
   (arguments
    ;; too lazy to add deps
    `(#:tests? #f))
   (propagated-inputs
    `(("ocaml-seq" ,ocaml-seq)
      ("ocaml-odoc" ,ocaml-odoc)))
   ;; (native-inputs
   ;;  `(("ocaml-qtest" ,ocaml-qtest)
   ;;    ("ocaml-qcheck" ,ocaml-qcheck)
   ;;    ("ocaml-ounit" ,ocaml-ounit)
   ;;    ("ocaml-iter" ,ocaml-iter)
   ;;    ("ocaml-gen" ,ocaml-gen)
      ;; ("ocaml-uutf" ,ocaml-uutf)))
   (synopsis
    "A modular, clean and powerful extension of the OCaml standard library")
   (description #f)
   (license license:bsd-2)))

;; Irmin and dependencies

(define-public ocaml-alcotest-lwt
  (package
    (inherit ocaml-alcotest)
    (name "ocaml-alcotest-lwt")
    (arguments
     `(#:package "alcotest-lwt"
       #:test-target "."
       ;; TODO fix tests
       #:tests? #f))
    (propagated-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-logs" ,ocaml-logs)))
    (native-inputs
     `(("ocaml-re" ,ocaml-re)
       ("ocaml-cmdliner" ,ocaml-cmdliner)))))

(define-public ocaml-ppx-deriving
  (package
    (name "ocaml-ppx-deriving")
    (version "5.2.1")
    (home-page "https://github.com/ocaml-ppx/ppx_deriving")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url home-page)
          (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "1wqcnw4wi6pfjjhixpakckm03dpj990259za432804471a6spm2j"))))
    (build-system dune-build-system)
    (arguments
     `(#:test-target "."))
    (propagated-inputs
      `(("ocaml-ppx-derivers" ,ocaml-ppx-derivers)
        ("ocaml-ppxlib" ,ocaml-ppxlib)
        ("ocaml-result" ,ocaml-result)))
    (native-inputs
      `(("ocaml-cppo" ,ocaml-cppo)
        ("ocaml-ounit2" ,ocaml-ounit2)))
    (properties `((upstream-name . "ppx_deriving")))
    (synopsis
      "Type-driven code generation for OCaml")
    (description
      "ppx_deriving provides common infrastructure for generating
code based on type definitions, and a set of useful plugins
for common tasks. ")
    (license license:expat)))

(define-public ocaml-bheap
  (package
    (name "ocaml-bheap")
    (version "2.0.0")
    (home-page "https://github.com/backtracking/bheap")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "0b8md5zl4yz7j62jz0bf7lwyl0pyqkxqx36ghkgkbkxb4zzggfj1"))))
    (build-system dune-build-system)
    (arguments `(#:test-target "."))
    (native-inputs
      `(("ocaml-stdlib-shims" ,ocaml-stdlib-shims)))
    (synopsis "Priority queues")
    (description
      "Traditional implementation using a binary heap encoded in a resizable array.")
    (license license:lgpl2.1)))

(define-public ocaml-ocamlgraph
  (package
    (name "ocaml-ocamlgraph")
    (version "2.0.0")
    (home-page "https://github.com/backtracking/ocamlgraph/")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "1gjrsyyamvvn2rd9n9yjx6hsglhw0dbm4cgazq0dpx0bbr4inwc3"))))
    (build-system dune-build-system)
    (arguments
     `(#:package "ocamlgraph"
       #:test-target "."))
    (propagated-inputs
     `(("ocaml-stdlib-shims" ,ocaml-stdlib-shims)
       ("ocaml-graphics" ,ocaml-graphics)))
    (synopsis "A generic graph library for OCaml")
    (description
      "Provides both graph data structures and graph algorithms")
    (license license:lgpl2.1)))

(define-public ocaml-uucd
  (package
    (name "ocaml-uucd")
    (version "13.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://erratique.ch/software/uucd/releases/uucd-13.0.0.tbz")
        (sha256
          (base32
            "1fg77hg4ibidkv1x8hhzl8z3rzmyymn8m4i35jrdibb8adigi8v2"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:build-flags (list "build")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (propagated-inputs `(("ocaml-xmlm" ,ocaml-xmlm)))
    (native-inputs
     `(("opam" ,opam)
       ("ocamlbuild" ,ocamlbuild)
       ("ocaml-topkg" ,ocaml-topkg)))
    (home-page "https://erratique.ch/software/uucd")
    (synopsis
      "Unicode character database decoder for OCaml")
    (description
      "Uucd is an OCaml module to decode the data of the Unicode character
database from its XML.  It provides high-level (but not necessarily efficient)
access to the data so that efficient representations can be extracted.")
    (license license:isc)))

(define-public ocaml-uucp
  (package
    (name "ocaml-uucp")
    (version "13.0.0")
    (source
      (origin
        (method url-fetch)
        (uri "https://erratique.ch/software/uucp/releases/uucp-13.0.0.tbz")
        (sha256
          (base32
            "19kf8ypxaakacgg1dwwfzkc2zicaj88cmw11fw2z7zl24dn4gyiq"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:build-flags (list "build")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (native-inputs
     `(("ocamlbuild" ,ocamlbuild)
       ("ocaml-topkg" ,ocaml-topkg)
       ("opam" ,opam)
       ("ocaml-uucd" ,ocaml-uucd)
       ("ocaml-uunf" ,ocaml-uunf)
       ("ocaml-uutf" ,ocaml-uutf)))
    (home-page "https://erratique.ch/software/uucp")
    (synopsis "Unicode character properties for OCaml")
    (description "Uucp is an OCaml library providing efficient access to a
selection of character properties of the Unicode character database.  Uucp is
independent from any Unicode text data structure and has no dependencies. It is
distributed under the ISC license.") 
    (license license:isc)))

(define-public ocaml-calendar
  (package
    (name "ocaml-calendar")
    (version "a447a88ae3c1e9873e32d2a95d3d3e7c5ed4a7da")
    (home-page "https://github.com/ocaml-community/calendar/")
    (source
      (origin
        (method git-fetch)
        (uri
         (git-reference
          (url home-page)
          (commit version)))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "09d9gyqm3zkf3z2m9fx87clqihx6brf8rnzm4yq7c8kf1p572hmc"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'allow-camlfind-destdir
           (lambda _
             (substitute* "Makefile"
               (("\\$\\(CAMLFIND\\) install") "mkdir -p $(DESTDIR) && $(CAMLFIND) install -destdir $(DESTDIR)"))
             #t)))
       #:make-flags
       (list (string-append "DESTDIR=" (assoc-ref %outputs "out")
                            "lib/ocaml/site-lib/"))))
    (native-inputs
     `(("ocaml-findlib" ,ocaml-findlib)
       ("ocaml" ,ocaml)
       ("autoconf" ,autoconf)
       ("automake" ,automake)))
    (synopsis "Library for handling dates and times in your program")
    (description #f)
    (license license:lgpl2.1)))

;; Following two packages are required to run tests for ocaml-eqaf.
;; ocaml-afl-persistent has no build-system and I don't know how to install it...

;; (define-public ocaml-afl-persistent
;;   (package
;;     (name "ocaml-afl-persistent")
;;     (version "1.3")
;;     (home-page "https://github.com/stedolan/ocaml-afl-persistent")
;;     (source
;;       (origin
;;         (method git-fetch)
;;         (uri (git-reference
;;               (url home-page)
;;               (commit (string-append "v" version))))
;;         (file-name (git-file-name name version))
;;         (sha256
;;           (base32
;;             "06yyds2vcwlfr2nd3gvyrazlijjcrd1abnvkfpkaadgwdw3qam1i"))))
;;     (build-system gnu-build-system)
;;     (native-inputs
;;      `(("ocaml", ocaml)
;;        ("autoconf" ,autoconf)
;;        ("automake" ,automake)))
;;     (synopsis "Use afl-fuzz in persistent mode")
;;     (description
;;       "afl-fuzz normally works by repeatedly fork()ing the program being
;; tested. using this package, you can run afl-fuzz in 'persistent mode',
;; which avoids repeated forking and is much faster.")
;; (license license:expat)))

;; (define-public ocaml-crowbar
;;   (package
;;     (name "ocaml-crowbar")
;;     (version "0.2")
;;     (source
;;       (origin
;;         (method url-fetch)
;;         (uri "https://github.com/stedolan/crowbar/archive/v0.2.tar.gz")
;;         (sha256
;;           (base32
;;             "02arkqv0xzmxmpcdmmki2r2bpdk3kzrgllnm36pmr8dw6gw52pjl"))))
;;     (build-system dune-build-system)
;;     (propagated-inputs
;;       `(("ocaml-ocplib-endian" ,ocaml-ocplib-endian)
;;         ("ocaml-cmdliner" ,ocaml-cmdliner)
;;         ("ocaml-afl-persistent" ,ocaml-afl-persistent)))
;;     (native-inputs
;;       `(("ocaml-calendar" ,ocaml-calendar)
;;         ("ocaml-fpath" ,ocaml-fpath)
;;         ("ocaml-uucp" ,ocaml-uucp)
;;         ("ocaml-uunf" ,ocaml-uunf)
;;         ("ocaml-uutf" ,ocaml-uutf)))
;;     (home-page "https://github.com/stedolan/crowbar")
;;     (synopsis
;;       "Write tests, let a fuzzer find failing cases")
;;     (description
;;       "Crowbar is a library for testing code, combining QuickCheck-style
;; property-based testing and the magical bug-finding powers of
;; [afl-fuzz](http://lcamtuf.coredump.cx/afl/).
;; ")
;;     (license #f)))

(define-public ocaml-eqaf
  (package
    (name "ocaml-eqaf")
    (version "0.7")
    (home-page "https://github.com/mirage/eqaf")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))
             ))
       (sha256
        (base32
         "06hsnnjax1kb3qsi3cj0nyyz8c2hj2gbw3h517gpjinpnwy2fr85"))))
    (build-system dune-build-system)
    ;; dependencies for test are hard to build (afl-persistent)
    (arguments `(#:tests? #f))
    (synopsis
     "Constant-time equal function on string")
    (description "This package provides an equal function on string in
constant-time to avoid timing-attack with crypto stuff.")
    (license license:expat)))

(define-public ocaml-digestif
  (package
    (name "ocaml-digestif")
    (version "1.0.0")
    (home-page "https://github.com/mirage/digestif")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit (string-append "v" version))))
        (sha256
          (base32
            "0x046by4myiksch16vyhj5l7xkflwhhxm8gzlf7474y0mw77w6lw"))))
    (build-system dune-build-system)
    (propagated-inputs
      `(("ocaml-eqaf" ,ocaml-eqaf)
        ("ocaml-bigarray-compat" ,ocaml-bigarray-compat)
        ("ocaml-stdlib-shims" ,ocaml-stdlib-shims)))
    (native-inputs
      `(("pkg-config" ,pkg-config)
        ("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-alcotest" ,ocaml-alcotest)
        ("ocaml-bos" ,ocaml-bos)
        ("ocaml-astring" ,ocaml-astring)
        ("ocaml-fpath" ,ocaml-fpath)
        ("ocaml-rresult" ,ocaml-rresult)
        ("ocaml-findlib" ,ocaml-findlib)))
    (synopsis "Hashes implementations (SHA*, RIPEMD160, BLAKE2* and MD5)")
    (description
      "Digestif is a toolbox to provide hashes implementations in C and OCaml.

It uses the linking trick and user can decide at the end to use the C implementation or the OCaml implementation.

We provides implementation of:
 * MD5
 * SHA1
 * SHA224\n * SHA256
 * SHA384
 * SHA512
 * BLAKE2B
 * BLAKE2S
 * RIPEMD160
")
    (license license:expat)))

(define-public ocaml-either
  (package
    (name "ocaml-either")
    (version "1.0.0")
    (home-page "https://github.com/mirage/either")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (sha256
          (base32
            "099p1m24vz5i0043zcfp88krzjsa2qbrphrm4bnx84gif5vgkxwm"))))
    (build-system dune-build-system)
    (arguments
     `(#:test-target "."))
    (synopsis "Compatibility Either module")
    (description "Projects that want to use the Either module defined in OCaml
4.12.0 while staying compatible with older versions of OCaml should use this
library instead.")
    (license license:expat)))

(define-public ocaml-repr
  (package
    (name "ocaml-repr")
    (version "0.3.0")
    (home-page "https://github.com/mirage/repr")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (commit version)))
        (sha256
          (base32
            "1pim6cpkn71x35k2nkdf5hbv1kinnxkpxi6ff7wwbvsh6kaxrq1g"))))
    (build-system dune-build-system)
    (arguments `(#:package "repr"))
    (propagated-inputs
      `(("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-uutf" ,ocaml-uutf)
        ("ocaml-either" ,ocaml-either)
        ("ocaml-jsonm" ,ocaml-jsonm)
        ("ocaml-base64" ,ocaml-base64)
        ("ocaml-odoc" ,ocaml-odoc)))
    (synopsis
      "Dynamic type representations.  Provides no stability guarantee")
    (description
      "This package defines a library of combinators for building dynamic type
representations and a set of generic operations over representable types, used
in the implementation of Irmin and related packages.  It is not yet intended for
public consumption and provides no stability guarantee.")
    (license license:isc)))

(define-public ocaml-ppx-repr
  (package
    (inherit ocaml-repr)
    (name "ocaml-ppx-repr")
    (arguments `(#:package "ppx_repr"
		 #:tests? #f))
    (propagated-inputs
      `(("ocaml-repr" ,ocaml-repr)
        ("ocaml-ppxlib" ,ocaml-ppxlib)
        ("ocaml-ppx-deriving" ,ocaml-ppx-deriving)
        ("ocaml-odoc" ,ocaml-odoc)))
    (native-inputs
      `(("ocaml-hex" ,ocaml-hex)
        ("ocaml-alcotest" ,ocaml-alcotest)))
    (properties `((upstream-name . "ppx_repr")))
    (synopsis "PPX deriver for type representations")
    (description "PPX deriver for type representations")
    (license license:isc)))

(define-public ocaml-ppx-irmin
  (package
    (name "ocaml-ppx-irmin")
    (version "2.6.1")
    (home-page "https://github.com/mirage/irmin")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (sha256
        (base32 "09q02yknb4w6rflhh4iwnz4hcmzapax6mn64phnfa2aab54v5h55"))))
    (build-system dune-build-system)
    (arguments `(#:package "ppx_irmin"))
    (propagated-inputs
     `(("ocaml-ppx-repr" ,ocaml-ppx-repr)))
    (synopsis "PPX deriver for Irmin type representations")
    (description "Irmin is a library for persistent stores with built-in
snapshot, branching and reverting mechanisms. It is designed to use a large
variety of backends. Irmin is written in pure OCaml and does not depend on
external C stubs; it aims to run everywhere, from Linux, to browsers and Xen
unikernels.")
    (license license:isc)))

(define-public ocaml-irmin
  (package
    (inherit ocaml-ppx-irmin)
    (name "ocaml-irmin")
    (arguments `(#:package "irmin"))
    (propagated-inputs
      `(("ocaml-repr" ,ocaml-repr)
        ("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-uri" ,ocaml-uri)
        ("ocaml-uutf" ,ocaml-uutf)
        ("ocaml-jsonm" ,ocaml-jsonm)
        ("ocaml-lwt" ,ocaml-lwt)
        ("ocaml-ppx-irmin" ,ocaml-ppx-irmin)
        ("ocaml-digestif" ,ocaml-digestif)
        ("ocamlgraph" ,ocaml-ocamlgraph)
        ("ocaml-logs" ,ocaml-logs)
        ("ocaml-bheap" ,ocaml-bheap)
        ("ocaml-astring" ,ocaml-astring)))
    (native-inputs
      `(("ocaml-hex" ,ocaml-hex)
        ("ocaml-alcotest" ,ocaml-alcotest)
        ("ocaml-alcotest-lwt" ,ocaml-alcotest-lwt)))
    (synopsis "Irmin, a distributed database that follows the same design
principles as Git")))

(define-public ocaml-irmin-fs
  (package
    (inherit ocaml-irmin)
    (name "ocaml-irmin-fs")
    (arguments `(#:package "irmin-fs"
                 ;; TODO: add tests
                 #:tests? #f))
    (propagated-inputs
      `(("ocaml-irmin" ,ocaml-irmin)
        ("ocaml-fmt" ,ocaml-fmt)
        ("ocaml-lwt" ,ocaml-lwt)
        ("ocaml-logs" ,ocaml-logs)
        ("ocaml-astring" ,ocaml-astring)))
    (synopsis "Unix backends for Irmin")
    (description "`Irmin_unix` defines Unix backends (including Git and HTTP) for Irmin, as well
as a very simple CLI tool (called `irmin`) to manipulate and inspect Irmin
stores.")))

(define-public ocaml-terminal-size
  (package
    (name "ocaml-terminal-size")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/cryptosense/terminal_size/releases/download/v0.1.4/terminal_size-v0.1.4.tbz")
       (sha256
        (base32
         "19viag98vmzjfyl1qrm3w9rb50k7bf8xj0xhba74lb47gpp1zjpx"))))
    (build-system dune-build-system)
    (native-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)))
    (properties `((upstream-name . "terminal_size")))
    (home-page
     "https://github.com/cryptosense/terminal_size")
    (synopsis "Get the dimensions of the terminal")
    (description
     "You can use this small library to detect the dimensions of the terminal window
attached to a process.
")
    (license #f)))

(define-public ocaml-progress
  (package
    (name "ocaml-progress")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/CraigFe/progress/releases/download/0.1.1/progress-0.1.1.tbz")
       (sha256
        (base32
         "0cjsg3b7bk8cpsz6jrqpsa0nkwzwpl46y0371dmlqjh1kp0vxilh"))))
    (build-system dune-build-system)
    (arguments `(;; TODO fix tests
                 #:tests? #f))
    (propagated-inputs
     `(("ocaml-mtime" ,ocaml-mtime)
       ("ocaml-terminal-size" ,ocaml-terminal-size)
       ("ocaml-odoc" ,ocaml-odoc)))
    (native-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-astring" ,ocaml-astring)
       ("ocaml-fmt" ,ocaml-fmt)))
    (home-page "https://github.com/CraigFe/progress")
    (synopsis "User-definable progress bars")
    (description
     "A progress bar library for OCaml, featuring a DSL for declaratively specifying
progress bar formats. Supports rendering multiple progress bars simultaneously.")
    (license #f)))

(define-public ocaml-semaphore-compat
  (package
    (name "ocaml-semaphore-compat")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/mirage/semaphore-compat/releases/download/1.0.1/semaphore-compat-1.0.1.tbz")
       (sha256
        (base32
         "139c5rxdp4dg1jcwyyxvhxr8213l1xdl2ab0mc288rfcppsiyxrb"))))
    (build-system dune-build-system)
    (arguments `(;; no tests
                 #:tests? #f))
    (home-page
     "https://github.com/mirage/semaphore-compat")
    (synopsis "Compatibility Semaphore module")
    (description
     "Projects that want to use the Semaphore module defined in OCaml 4.12.0 while
staying compatible with older versions of OCaml should use this library
instead. ")
    (license #f)))

(define-public ocaml-index
  (package
    (name "ocaml-index")
    (version "1.3.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/mirage/index/releases/download/1.3.1/index-1.3.1.tbz")
       (sha256
        (base32
         "1qlyc9z144mi1j4312djfwzxc88lz6hn4qral5jn982b67yn5in9"))))
    (build-system dune-build-system)
    (arguments `(#:package "index"
                 ;; TODO: add tests
                 #:tests? #f))
    (propagated-inputs
     `(("ocaml-repr" ,ocaml-repr)
       ("ocaml-ppx-repr" ,ocaml-ppx-repr)
       ("ocaml-fmt" ,ocaml-fmt)
       ("ocaml-logs" ,ocaml-logs)
       ("ocaml-mtime" ,ocaml-mtime)
       ("ocaml-cmdliner" ,ocaml-cmdliner)
       ("ocaml-progress" ,ocaml-progress)
       ("ocaml-semaphore-compat" ,ocaml-semaphore-compat)
       ("ocaml-jsonm" ,ocaml-jsonm)
       ("ocaml-stdlib-shims" ,ocaml-stdlib-shims)))
    (native-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ;; ("ocaml-crowbar" ,ocaml-crowbar)
       ("ocaml-re" ,ocaml-re)))
    (home-page "https://github.com/mirage/index")
    (synopsis
     "A platform-agnostic multi-level index for OCaml")
    (description
     "Index is a scalable implementation of persistent indices in OCaml.

It takes an arbitrary IO implementation and user-supplied content
types and supplies a standard key-value interface for persistent
storage. Index provides instance sharing: each OCaml
run-time can share a common singleton instance.

Index supports multiple-reader/single-writer access. Concurrent access
is safely managed using lock files.")
    (license license:expat)))

(define-public ocaml-irmin-layers
  (package
   (inherit ocaml-irmin)
   (name "ocaml-irmin-layers")
   (arguments `(#:package "irmin-layers"
                ;; TODO: add tests
                #:tests? #f))
   (propagated-inputs
    `(("ocaml-mtime" ,ocaml-mtime)
      ("ocaml-irmin" ,ocaml-irmin)
      ("ocaml-logs" ,ocaml-logs)
      ("ocaml-lwt" ,ocaml-lwt)))
   (synopsis "Combine different Irmin stores into a single, layered store")
   (description #f)))

(define-public ocaml-irmin-pack
  (package
   (inherit ocaml-irmin)
   (name "ocaml-irmin-pack")
   (arguments `(#:package "irmin-pack"
                ;; TODO: add tests
                #:tests? #f))
   (propagated-inputs
    `(("ocaml-irmin" ,ocaml-irmin)
      ("ocaml-irmin-layers" ,ocaml-irmin-layers)
      ("ocaml-ppx-irmin" ,ocaml-ppx-irmin)
      ("ocaml-index" ,ocaml-index)
      ("ocaml-fmt" ,ocaml-fmt)
      ("ocaml-logs" ,ocaml-logs)
      ("ocaml-lwt" ,ocaml-lwt)
      ("ocaml-mtime" ,ocaml-mtime)
      ("ocaml-cmdliner" ,ocaml-cmdliner)))
   (home-page "https://github.com/mirage/irmin")
   (synopsis
    "Irmin backend which stores values in a pack file")
   (description #f)
   (license license:isc)))

(define-public ocaml-cryptokit
  (package
   (name "ocaml-cryptokit")
   (version "1.16.1")
   (home-page "https://github.com/xavierleroy/cryptokit")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url home-page)
           (commit "release1161")))
     (sha256
      (base32
       "1w1iarhf1rhpkmbmjjpxsfsq55cdd9azwb6s12bzk5w0bs5k6ckq"))))
   (build-system dune-build-system)
   (propagated-inputs
    `(("ocaml-zarith" ,ocaml-zarith)
      ("zlib" ,zlib)
      ("gmp" ,gmp)))
   (synopsis
    "A library of cryptographic primitives")
   (description
    "Cryptokit includes block ciphers (AES, DES, 3DES), stream ciphers
(ARCfour), public-key crypto (RSA, DH), hashes (SHA-1, SHA-256,
SHA-3), MACs, compression, random number generation -- all presented
with a compositional, extensible interface.")
   (license license:gpl2)))


(define %source-dir (dirname (current-filename)))

(define-public ocaml-dmc
  (package
    (name "ocaml-dmc")
    (version "0.0.0")
    (source (local-file %source-dir #:recursive? #t))
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     `(("ocaml-eris" ,ocaml-eris)
       ("ocaml-rdf" ,ocaml-rdf)
       ("ocaml-base32" ,ocaml-base32)
       ("ocaml-base64" ,ocaml-base64)
       ("ocaml-cmdliner" ,ocaml-cmdliner)
       ("ocaml-containers" ,ocaml-containers)
       ("ocaml-cryptokit" ,ocaml-cryptokit)
       ("ocaml-datalog" ,ocaml-datalog)
       ("ocaml-irmin" ,ocaml-irmin)
       ("ocaml-irmin-pack" ,ocaml-irmin-pack)
       ("ocaml-logs" ,ocaml-logs)))
    (native-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-alcotest-lwt" ,ocaml-alcotest-lwt)
       ;; dune requires git to figure out version when installing
       ("git" ,git)))
    (home-page "https://gitlab.com/public.dream/DROMEDAR/ocaml-dmc")
    (synopsis "OCaml library and CLI tool for Distributed Mutable Containers (DMC)")
    (description "sdfsdf")
    (license license:agpl3+)))

ocaml-dmc
