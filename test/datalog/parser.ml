(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.netq>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Store = Irmin_mem.KV(Irmin.Contents.String)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)
module Datalog = Dmc.Datalog

module Parser = Datalog.Parser
module Term = Datalog.Term
module Atom = Datalog.Atom
module Literal = Datalog.Literal
module Clause = Datalog.Clause

open Alcotest

let parse p =
  Angstrom.parse_string
    ~consume:Angstrom.Consume.All
    p

let xsd_string_test_case =
  test_case "xsd_string" `Quick
    (fun () ->
       check (result Rdf_alcotest.literal string)
         "can parse"
         (parse Parser.xsd_string "\"Hello World\"")
         (Result.ok @@ Rdf.Literal.make "Hello World" @@ Rdf.Namespace.xsd "string")
    )

let iri_test_case =
  test_case "iri" `Quick
    (fun () ->
       check (result Rdf_alcotest.iri string)
         "can parse"
         (parse Parser.iri "<http://example.com>")
         (Result.ok @@ Rdf.Iri.of_string "http://example.com")
    )

let variable_test_case =
  test_case "variable" `Quick
    (fun () -> check (result int string)
        "can parse"
        (parse Parser.variable "?42")
        (Result.ok @@ 42))

let term_testable =
  testable Term.pp Term.equal

let term_test_case =
  let cases =
    ["?1", Term.variable 1;
     "?2", Term.variable 2;
     "<http://example.com>", "http://example.com" |> Rdf.Iri.of_string |> Rdf.Term.of_iri |> Term.constant;
     "\"blups\"", Rdf.Literal.make "blups" (Rdf.Namespace.xsd "string") |> Rdf.Term.of_literal |> Term.constant;
    ] in

  test_case "term" `Quick
    (fun () ->
       List.iter
         (fun (enc, v) ->
            check (result term_testable string)
              "can parse"
              (parse Parser.term enc)
              (Result.ok v))
         cases)

let atom_testable =
  testable Atom.pp Atom.equal

let literal_test_case =
  let cases = [
    "p(?1,?2)", Atom.make "p" [Term.variable 1; Term.variable 2];
    "graph(?1,<http://example.com>)", Atom.make "graph" [Term.variable 1;
                                                         "http://example.com" |> Rdf.Iri.of_string |> Rdf.Term.of_iri |> Term.constant]
  ] in
  test_case "literal" `Quick
    (fun () ->
       List.iter
         (fun (enc, v) ->
            check (result atom_testable string)
              "can parse"
              (parse Parser.atom enc)
              (Result.ok v))
         cases)

let clause_testable =
  testable Clause.pp Clause.equal

let clause_test_case =
  let cases = [
    "p(?1, ?2).", Atom.make "p" [Term.variable 1; Term.variable 2] |> Clause.make_fact;
    "p(?1, ?2) :- q(?1,?2).", Clause.make
      (Atom.make "p" [Term.variable 1; Term.variable 2])
      [Literal.make_positive (Atom.make "q" [Term.variable 1; Term.variable 2])] ;
  ] in
  test_case "clause" `Quick
    (fun () ->
       List.iter
         (fun (enc, v) ->
            check (result clause_testable string)
              "can parse"
              (parse Parser.clause enc)
              (Result.ok v))
         cases)

let () =
  Alcotest.run "Dmc_datalog.Parser"
    [
      "Basic parsers", [
        xsd_string_test_case;
        iri_test_case;
        variable_test_case;
        term_test_case;
        literal_test_case;
        clause_test_case;
      ]
    ]
