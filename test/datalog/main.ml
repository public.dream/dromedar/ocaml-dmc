(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.netq>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)


module Store = Irmin_mem.KV(Irmin.Contents.String)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)
module Datalog = Dmc.Datalog

module Parser = Datalog.Parser
module Term = Datalog.Term
module Atom = Datalog.Atom
module Literal = Datalog.Literal
module Clause = Datalog.Clause
module DB = Datalog.DB

let parse p string =
  Angstrom.parse_string
    ~consume:Angstrom.Consume.All
    p string
  |> Result.get_ok

let parse_db = parse Parser.db
let parse_atom = parse Parser.atom
let parse_query = parse Parser.query

(* Computes the transitive closure in the predicate `path`. *)
let path_example =
  {example|

  edge("a", "b").
  edge("b", "c").
  edge("c", "d").

  path(?1,?2) :- edge(?1,?2).
  path(?1,?3) :- edge(?1,?2), path(?2,?3).
|example}

let path_test_case =
  Alcotest.test_case "Transitive closure" `Quick
    (fun () ->
       let db = parse_db path_example in
       let query = parse_query {q|path("a",?1)?|q} in
       let results = DB.prove db query in
       let path_a_d = parse_atom {a|path("a","d")|a} in
       Alcotest.(check int "number of results"
                   3 (List.length results));
       Alcotest.(check bool "path(a,d) in results"
                   true (List.mem path_a_d results)))

(* This example is taken from "A New Formulation of Tabled Resolution with Delay (1999)". *)
let negation_example =
  {example|

    p("b").
    p("c") :- not p("a").

    p(?1) :- t(?1, ?2, ?3), not p(?2), not p(?3).

    t("a", "b", "a").
    t("a", "a", "b").
|example}

let negation_test_case =
  Alcotest.test_case "Negation" `Quick
    (fun () ->
       let db = parse_db negation_example in
       let query = parse_query {q|p(?1)?|q} in
       let results = DB.prove db query in
       let p_a = parse_atom {a|p("a")|a} in
       let p_c = parse_atom {a|p("c")|a} in
       Alcotest.(check int "number of results"
                   2 (List.length results));
       Alcotest.(check bool "p(c) in results"
                   true (List.mem p_c results));
       Alcotest.(check bool "p(a) not in results"
                   false (List.mem p_a results)))

let ex =
  Rdf.Namespace.make_namespace "http://example.com/"

let rdf_signify_valid_test_case =
  Alcotest.test_case "valid signature" `Quick
    (fun () ->
       let db = DB.create () in
       let query =
         Atom.make "ed25519Verify" [
           Rdf.Literal.make "5XeoDhg1j45acr/8TmlSgKd6M5E8Wyz3zGw4iEyLJQeYHmAb4nHIrUnFXwEHvCfme6QZTrDDRINqRjJGciPmAQ=="
             (Rdf.Namespace.xsd "base64Binary") |> Rdf.Term.of_literal |> Term.constant;
           ex "hi" |> Rdf.Term.of_iri |> Term.constant;
           "urn:ed25519:pk:ZKM5HQMXNTAI4TLWJXZIJHTX3XJSS4O2QTAEWKIMCSLV7424WETA"
           |> Rdf.Iri.of_string |> Rdf.Term.of_iri |> Term.constant
         ] in
       let results = DB.prove db query in
       Alcotest.(check int "number of results" 1 (List.length results)))

let rdf_signify_invalid_test_case =
  Alcotest.test_case "invalid signature" `Quick
    (fun () ->
       let db = DB.create () in
       let query =
         Atom.make "ed25519Verify" [
           Rdf.Literal.make "5XdoDhg1j45acr/8TmlSgKd6M5E8Wyz3zGw4iEyLJQeYHmAb4nHIrUnFXwEHvCfme6QZTrDDRINqRjJGciPmAQ=="
             (Rdf.Namespace.xsd "base64Binary") |> Rdf.Term.of_literal |> Term.constant;
           ex "hi" |> Rdf.Term.of_iri |> Term.constant;
           "urn:ed25519:pk:ZKM5HQMXNTAI4TLWJXZIJHTX3XJSS4O2QTAEWKIMCSLV7424WETA"
           |> Rdf.Iri.of_string |> Rdf.Term.of_iri |> Term.constant
         ] in
       let results = DB.prove db query in
       Alcotest.(check int "number of results" 0 (List.length results)))

let () =
  Alcotest.run "Dmc_datalog"
    [ "Examples", [
          path_test_case;
          negation_test_case;
        ];
      "Rdf_signify interpreter", [
        rdf_signify_valid_test_case;
        rdf_signify_invalid_test_case;
      ]
    ]
