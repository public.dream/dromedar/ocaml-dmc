(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Store = Irmin_mem.KV(Irmin.Contents.String)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)

open Lwt.Infix
open Lwt.Syntax


let config = Irmin_mem.config ()

let ex = Rdf.Namespace.make_namespace "http://example.com/"

let element = "urn:erisx2:BIAET72CBID5IZEYNRXJGVNTX3CVVGQ26ZC4DN5WRLAY4OVCLSCVIU4A5MOXYXCPLSVINSFXIDRIYGVF22STQAVU7J3EPOOW2YOAN5FDHQ"
              |> Rdf.Iri.of_string
              |> Dmc.Eris.of_iri_exn

let initialize_test_case =
  Alcotest_lwt.test_case "initialize a new set" `Quick
    (fun _switch () ->
       let* store = Store.Repo.v config >>= Store.master in
       let* container_id, _secret_key, tree =
         Dmc.(get_tree store >>= Set.initialize)
       in
       let* () = Dmc.commit_tree store tree in
       let* replicas = Dmc.(get_tree store >>= replicas) in
       assert (List.mem container_id replicas);
       Lwt.return_unit)

let add_test_case =
  Alcotest_lwt.test_case "add an element to the set" `Quick
    (fun _switch () ->
       let* store = Store.Repo.v config >>= Store.master in
       let* container_id, secret_key, tree =
         Dmc.(get_tree store >>= Set.initialize)
       in
       let* _op, tree = Dmc.(
           tree
           |> Set.add_ref container_id ~secret_key ~element
         )
       in
       let* () = Dmc.commit_tree store tree in
       let* members = Dmc.(
           get_tree store
           >>= Set.members container_id)
       in
       assert (List.mem element members);
       Lwt.return_unit)

let remove_test_case =
  Alcotest_lwt.test_case "add and remove an element from a set" `Quick
    (fun _switch () ->
       let* store = Store.Repo.v config >>= Store.master in

       (* initialize a set *)
       let* container_id, secret_key, tree =
         Dmc.(get_tree store >>= Set.initialize)
       in
       let* () = Dmc.commit_tree store tree in

       (* add *)
       let* add_op, tree = Dmc.(
           get_tree store
           >>= Set.add_ref container_id ~secret_key ~element
         )
       in
       let* () = Dmc.commit_tree store tree in

       (* remove *)
       let* _remove_op, tree = Dmc.(
           get_tree store
           >>= Set.remove container_id ~secret_key ~operation:add_op
         )
       in
       let* () = Dmc.commit_tree store tree in

       let* members = Dmc.(
           get_tree store
           >>= Set.members container_id)
       in
       assert (not (List.mem element members));
       Lwt.return_unit)

let () =
  Lwt_main.run @@
  Alcotest_lwt.run "Dmc.Set"
    ["Intialize", [initialize_test_case];
     "Operations" , [add_test_case; remove_test_case]
    ]
