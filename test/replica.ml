(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* The in-memory Irmin store used for testing *)
module Store = Irmin_mem.Make
    (Irmin.Metadata.None)
    (Irmin.Contents.String)
    (Irmin.Path.String_list)
    (Irmin.Branch.String)
    (Irmin.Hash.BLAKE2B)

module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)

open Lwt.Infix
open Lwt.Syntax

let config = Irmin_mem.config ()

let ex = Rdf.Namespace.make_namespace "http://example.com/"

let container_id =
  "dmc:BIAD3RMUCOD3CH3RO6FQFJ6465ZDLXJCGGBZRWH4HFKM4YEGOOW5C3X56MMTEOE7VWCBFSGBVQQMYTTAQHMT6KTD7KYB3A3RNPP7JVUR4M"
  |> Rdf.Iri.of_string
  |> Dmc.ContainerIdentifier.of_iri_exn


let add_object_test_case =
  Alcotest_lwt.test_case "add an empty object" `Quick
    (fun _switch () ->
       let* store = Store.Repo.v config >>= Store.master in
       let fragment_graph = Dmc.FragmentGraph.empty in
       let iri = Dmc.FragmentGraph.base_subject fragment_graph in
       let* _urn, tree =
         Dmc.(get_tree store
              >>= Objects.add container_id fragment_graph) in
       let* () = Dmc.commit_tree store tree in
       let* objects = Dmc.(
           get_tree store
           >>= Objects.to_list container_id
           >|= List.map Dmc.Eris.to_iri
         )
       in
       Alcotest.(check (list Rdf_alcotest.iri) "objects contains base subject"
                   [iri]
                   objects)
       |> Lwt.return)

let get_object_test_case =
  Alcotest_lwt.test_case "add and get an object" `Quick
    (fun _switch () ->
       let* store = Store.Repo.v config >>= Store.master in
       let fragment_graph = Dmc.FragmentGraph.(
           empty
           |> add_statement
             (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
             (Object.of_iri @@ ex "foo")
           |> add_statement
             (Predicate.of_iri @@ ex "prop")
             (Object.of_iri @@ ex "bar"))
       in
       let urn = Dmc.FragmentGraph.base_subject fragment_graph
                 |> Dmc.Eris.of_iri_exn in
       let* _urn, tree  =
         Dmc.(get_tree store
              >>= Objects.add container_id fragment_graph)
       in
       let* () = Dmc.commit_tree store tree in
       let* object' = Dmc.(get_tree store
                           >>= Eris.get_object urn
                           >|= Result.get_ok)
       in
       assert (Dmc.FragmentGraph.equal fragment_graph
                 object');
       Lwt.return_unit)

let () =
  Lwt_main.run @@
  Alcotest_lwt.run "Dmc replica tests"
    ["Objects", [add_object_test_case;
                 get_object_test_case
                ]]
