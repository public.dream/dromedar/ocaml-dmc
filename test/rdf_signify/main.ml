(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Store = Irmin_mem.KV(Irmin.Contents.String)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)
module Signify = Dmc.Signify


let ex =
  Rdf.Namespace.make_namespace "http://example.com/"

let rdf_signify_test_case =
  Alcotest.test_case "sign and verify" `Quick
    (fun () ->
       let secret_key = Signify.generate_secret_key () in
       (* Format.printf "%a\n" Rdf.Iri.pp secret_key; *)
       let public_key = Signify.public_key secret_key in
       (* Format.printf "%a\n" Rdf.Iri.pp public_key; *)
       let message = ex "hi" in
       let signature = Signify.signature_value ~secret_key message in
       (* Format.printf "%a\n" Rdf.Literal.pp signature; *)
       assert (Signify.verify
                 ~public_key:public_key
                 ~signature:(Rdf.Term.of_literal signature)
                 (Rdf.Term.of_iri message)))

let () =
  Alcotest.run "Rdf_signify"
    [ "Random example", [ rdf_signify_test_case ] ]
