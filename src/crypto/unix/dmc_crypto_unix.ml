(* Cryptographic primitives for Unix *)


let random_string len =
  Cryptokit.Random.(string secure_rng len)

module Blake2b = Monocypher.Hashing.Blake2b
module IETF_ChaCha20 = Monocypher.Advanced.IETF_ChaCha20
module Ed25519 = Monocypher.Optional.Ed25519
