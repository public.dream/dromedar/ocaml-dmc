(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0
 *)


(** DMC CBOR serialization *)

(** This module provides functions for emiting and loading CBOR serializations of a DMC replica state. *)

(** @see <http://purl.org/dmc#_cbor_serialization> Specification of the CBOR serialization *)


module type S = sig

  module Dmc : Dmc.S

  type tree

  val to_cbor : Dmc.ContainerIdentifier.t -> ?objects:bool -> ?blocks:bool -> tree -> Cbor.t Lwt.t
  (** [to_cbor container_id ~objects ~blocks tree] returns a CBOR serialization
      of the replica state. If [objects] is false, replica objects are not included.
      If [blocks] is false, replica blocks are not included. *)

  val to_binary : Dmc.ContainerIdentifier.t -> ?objects:bool -> ?blocks:bool -> tree -> string Lwt.t
  (** [to_binary container_id ~objects ~blocks tree] returns a CBOR
      serialization (as string) of the replica state. If [objects] is false,
      replica objects are not included.If [blocks] is false, replica blocks are
      not included. *)

  val load : string -> tree -> tree Lwt.t
  (** [load dump tree] loads the CBOR dump of the replica in [dump] into the [tree]. *)
end

module Make(Dmc : Dmc.S): S with type tree = Dmc.tree and module Dmc = Dmc
