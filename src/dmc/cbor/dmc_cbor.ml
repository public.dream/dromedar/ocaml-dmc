(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0
 *)

open Lwt.Infix
open Lwt.Syntax

let src = Logs.Src.create "dmc.cbor"
module Log = (val Logs.src_log src : Logs.LOG)

module type S = sig
  (** DMC CBOR serialization *)

  (** This module provides functions for emiting and loading CBOR serializations of a DMC replica state. *)

  (** @see http://purl.org/dmc#_cbor_serialization Specification of the CBOR serialization *)

  module Dmc : Dmc.S

  type tree

  val to_cbor : Dmc.ContainerIdentifier.t -> ?objects:bool -> ?blocks:bool -> tree -> Cbor.t Lwt.t
  (** [to_cbor container_id ~objects ~blocks tree] returns a CBOR serialization
      of the replica state. If [objects] is false, replica objects are not included.
      If [blocks] is false, replica blocks are not included. *)

  val to_binary : Dmc.ContainerIdentifier.t -> ?objects:bool -> ?blocks:bool -> tree -> string Lwt.t
  (** [to_binary container_id ~objects ~blocks tree] returns a CBOR
      serialization (as string) of the replica state. If [objects] is false,
      replica objects are not included.If [blocks] is false, replica blocks are
      not included. *)

  val load : string -> tree -> tree Lwt.t
  (** [load dump tree] loads the CBOR dump of the replica in [dump] into the [tree]. *)
end

module Make(Dmc : Dmc.S) = struct

  module Dmc = Dmc

  type tree = Dmc.tree

  let to_cbor container_id ?(objects=true) ?(blocks=true) tree =
    (* encode the container id as CBOR *)
    let container_id_cbor =
      Dmc.ContainerIdentifier.to_eris_urn container_id
      |> Dmc.Eris.to_cbor
    in

    (* get replica objects *)
    let* objects_cbor =
      if objects then
        (* get list of objects *)
        Dmc.Objects.to_list container_id tree
        (* convert ERIS urns to CBOR *)
        >|= List.map Dmc.Eris.to_cbor
        >|= (fun l -> Cbor.Array l)
        >|= Option.some
      else
        Lwt.return_none
    in

    (* get blocks *)
    let* blocks_cbor =
      if blocks then
        (* get list of replica block references *)
        Dmc.BlockReferences.to_list container_id tree
        >>= Lwt_list.map_p (fun reference ->
            (* attempt to get block from block storage *)
            let* block_opt = Dmc.Blocks.get tree reference in
            match block_opt with
            | Some block ->
              (* encode as Cbor pair *)
              (Cbor.ByteString reference, Cbor.ByteString block)
              |> Lwt.return
            | None ->
              (* fail if block can not be retrieved *)
              Log.err (fun m -> m "replica block missing (ref: %a)"
                          Fmt.(hex ~w:4 () |> on_string)
                          reference);
              Lwt.fail_with "block missing")
        (* encode as CBOR map *)
        >|= (fun m -> Cbor.Map m)
        >|= Option.some
      else
        Lwt.return_none
    in

    (* Combine everything *)
    [Some container_id_cbor;
     objects_cbor;
     blocks_cbor]
    |> List.filter_map (fun x -> x)
    |> (fun l -> Cbor.Array l)
    |> Lwt.return

  let to_binary container_id ?(objects=true) ?(blocks=true) tree =
    to_cbor container_id ~objects ~blocks tree
    >|= Cbor.encode

  let cbor_parser =
    let open Angstrom in
    let open Cbor.Parser in

    (* parses a ERIS Urn from CBOR *)
    let eris_urn_parser =
      Dmc.Eris.cbor_parser
    in

    let container_id_parser =
      Dmc.Eris.cbor_parser
      >>| Dmc.ContainerIdentifier.of_eris_urn
    in

    (* parsers a list of ERIS URNs *)
    let objects_parser = array eris_urn_parser in
    let blocks_parser = map any_byte_string any_byte_string in

    choice
      [(* A CBOR dump containing everything *)
        triple container_id_parser objects_parser blocks_parser
        >>| (fun (container_id, objects, blocks) ->
            Log.info (fun m -> m "Loading objects and blocks for container %a"
                         Dmc.ContainerIdentifier.pp container_id
                     );
            container_id, Some objects, Some blocks);

        (* A CBOR dump containing withtout blocks *)
        tuple container_id_parser objects_parser
        >>| (fun (container_id, objects) ->
            Log.info (fun m -> m "Loading objects for container %a"
                         Dmc.ContainerIdentifier.pp container_id);
            container_id, Some objects, None);

        (* A CBOR dump containing withtout objects *)
        tuple container_id_parser blocks_parser
        >>| (fun (container_id, blocks) ->
            Log.info (fun m -> m "Loading blocks for container %a"
                         Dmc.ContainerIdentifier.pp container_id);
            container_id, None, Some blocks);

        (* A CBOR dump that only contains the container id. valid but useless. *)
        array container_id_parser
        >>= (function
            | [container_id] ->
              (container_id, None, None)
              |> return
            | _ ->
              fail "invalid DMC CBOR")]

  let load s tree =

    let add_objects container_id objects tree =
      Lwt_list.fold_left_s
        (fun tree object' -> Dmc.Objects.add_reference container_id object' tree)
        tree
        objects
    in

    let add_blocks container_id blocks tree =
      Lwt_list.fold_left_s
        (fun tree (reference, block) ->
           tree
           |> Dmc.Blocks.add reference block
           >>= Dmc.BlockReferences.add container_id reference)
        tree
        blocks
    in

    match Angstrom.parse_string ~consume:Angstrom.Consume.All cbor_parser s with
    | Ok (container_id, Some objects, Some blocks) ->
      tree
      |> add_objects container_id objects
      >>= add_blocks container_id blocks

    | Ok (container_id, Some objects, None) ->
      tree
      |> add_objects container_id objects

    | Ok (container_id, None, Some blocks) ->
      tree
      |> add_blocks container_id blocks

    | Ok (_, None, None) ->
      Log.warn (fun m -> m "CBOR dump does not contain anything that can be added.");
      Lwt.return tree

    | Error msg ->
      Log.err (fun m -> m "Can not decode CBOR (%s)" msg);
      Lwt.fail_invalid_arg "invalid CBOR"


end
