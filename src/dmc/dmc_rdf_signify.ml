(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Iri = Rdf.Iri

let src = Logs.Src.create "dmc.rdf_signify"
module Log = (val Logs.src_log src : Logs.LOG)

module type S = sig
  (** RDF Signify

      A RDF vocabulary for cryptographically signing content-addressed content
      using the Ed25519 algorithm.

  *)

  (** @see <http://purl.org/signify> RDF Signify specification *)


  exception Invalid_key
  (** The exception raised when a key can not be decoded from the given
      IRI. *)

  module PublicKey: sig

    type t

    val of_iri: Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val to_iri: t -> Rdf.Iri.t

    val pp : t Fmt.t
  end

  module SecretKey : sig

    type t

    val of_iri: Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val to_iri: t -> Rdf.Iri.t

    val pp : t Fmt.t
  end

  module FragmentGraph : Rdf_fragment_graph.S

  module Namespace : sig
    val public_key : FragmentGraph.Predicate.t
    val message : FragmentGraph.Predicate.t
    val signature : FragmentGraph.Object.t
  end

  val generate_secret_key : unit -> SecretKey.t
  (** [generate_secret_key ()] generates a new secret key (using
      {!Crypto.random_string}) and returns it as iri.*)

  val public_key : SecretKey.t -> PublicKey.t
  (** [public_key secret_key] returns the associated public key of [secret_key].*)

  val signature_value : secret_key:SecretKey.t -> Rdf.Iri.t -> Rdf.Literal.t
  (** [signature_exn ~secret_key message] creates a RDF Signify signature of
      [message] using [secret_key].*)

  val signature : secret_key:SecretKey.t -> Rdf.Iri.t -> FragmentGraph.t
  (** [signature_exn ~secret_key message] creates a RDF Signify signature of
      [message] using [secret_key].*)

  val verify: public_key:PublicKey.t -> signature:Rdf.Term.t -> Rdf.Term.t -> bool
  (** [verify ~public_key ~signature message] verifies if [signature] of [message]
      is valid for [public_key].*)
end


module Make(Crypto: Dmc_crypto.S)(FragmentGraph: Rdf_fragment_graph.S) = struct


  exception Invalid_key

  module PublicKey = struct

    type t = PublicKey of string

    let of_iri iri =
      let iri_string = Iri.to_string iri in
      if String.equal
          (Str.string_before iri_string 15)
          "urn:ed25519:pk:" then
        Base32.decode (Str.string_after iri_string 15)
        |> Result.to_option
        |> Option.map (fun k -> PublicKey k)
      else
        None

    let of_iri_exn iri =
      match of_iri iri with
      | Some key -> key
      | _ -> raise Invalid_key

    let to_iri = function
      | PublicKey key ->
        "urn:ed25519:pk:" ^ (Base32.encode_string ~pad:false key)
        |> Iri.of_string

    let pp ppf key =
      Fmt.pf ppf "%a"
        Rdf.Iri.pp
        (to_iri key)
  end

  module SecretKey = struct

    type t = SecretKey of string

    let of_iri iri =
      let iri_string = Iri.to_string iri in
      if String.equal
          (Str.string_before iri_string 15)
          "urn:ed25519:sk:" then
        Base32.decode (Str.string_after iri_string 15)
        |> Result.to_option
        |> Option.map (fun k -> SecretKey k)
      else
        None

    let of_iri_exn iri =
      match of_iri iri with
      | Some key -> key
      | _ -> raise Invalid_key

    let to_iri = function
      | SecretKey key ->
        "urn:ed25519:sk:" ^ (Base32.encode_string ~pad:false key)
        |> Iri.of_string

    let pp ppf key =
      Fmt.pf ppf "%a"
        Rdf.Iri.pp
        (to_iri key)
  end

  module FragmentGraph = FragmentGraph

  module Namespace = struct
    let signify = Rdf.Namespace.make_namespace "http://purl.org/signify/ns#"

    let signify_p s =
      signify s
      |> FragmentGraph.Predicate.of_iri

    let signify_o s =
      signify s
      |> FragmentGraph.Object.of_iri

    let public_key =
      signify_p "publicKey"

    let message =
      signify_p "message"

    let signature =
      signify_o "Signature"
  end

  let generate_secret_key () =
    SecretKey.SecretKey (Crypto.random_string 32)

  let public_key = function
    | SecretKey.SecretKey secret_key ->
      PublicKey.PublicKey (Crypto.Ed25519.public_key ~secret_key)

  let signature_value ~secret_key message =
    match secret_key with
    | SecretKey.SecretKey secret_key ->
      let signature = Crypto.Ed25519.sign ~secret_key (Iri.to_string message) in
      Rdf.Literal.make
        (Base64.encode_string signature)
        (Rdf.Namespace.xsd "base64Binary")

  let signature ~secret_key msg =
    let open FragmentGraph.Namespace in
    let open Namespace in
    FragmentGraph.(
      empty
      |> add_statement a signature
      |> add_statement message (Object.of_iri msg)
      |> add_statement value
        (Object.of_literal @@ signature_value ~secret_key msg))

  let decode_signature literal =
    let datatype = Rdf.Literal.datatype literal in
    if Iri.equal datatype (Rdf.Namespace.xsd "base64Binary") then
      Rdf.Literal.canonical literal
      |> Base64.decode
      |> Result.to_option
    else
      None

  let verify ~public_key ~signature message =
    Log.debug (fun m -> m "verify public_key:%a signature:%a %a"
                  PublicKey.pp public_key
                  Rdf.Term.pp signature
                  Rdf.Term.pp message);
    match public_key,
          Option.bind (Rdf.Term.to_literal signature) decode_signature,
          Option.map Iri.to_string (Rdf.Term.to_iri message)
    with
    | PublicKey.PublicKey public_key, Some signature, Some message ->
      Crypto.Ed25519.check ~signature ~public_key:public_key message
    | _ ->
      false

end
