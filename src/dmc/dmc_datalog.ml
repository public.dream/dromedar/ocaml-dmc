(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Triple = Rdf.Triple

module type S = sig
  module Term : sig
    type t
    val variable : int -> t
    val constant: Rdf.Term.t -> t

    val constant_value : t -> Rdf.Term.t option

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  type predicate = string

  module Atom: sig
    type t
    val make : predicate -> Term.t list -> t

    val predicate : t -> predicate
    val terms : t -> Term.t list

    val equal : t -> t -> bool

    val triple : Rdf.Triple.t -> t

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module Literal : sig
    type t

    val make_positive : Atom.t -> t
    val make_negative : Atom.t -> t

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module Clause : sig
    type t
    val make : Atom.t -> Literal.t list -> t
    val make_fact : Atom.t -> t

    (* val head : t -> Literal.t
     * val body : t -> Literal.t list *)

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module DB: sig
    type t
    val create : unit -> t

    val add_clause: t -> Clause.t -> unit
    val add_triple : t -> Rdf.Triple.t -> unit

    type query = Atom.t
    val prove : t -> query -> Atom.t list
  end

  module Parser : sig

    (** Parsers for Datalog programs. *)

    (** Note that this may be better moved to ocaml-dmc. *)

    val xsd_string : Rdf.Literal.t Angstrom.t
    val iri : Rdf.Iri.t Angstrom.t

    val constant : Rdf.Term.t Angstrom.t
    val variable : int Angstrom.t

    val term : Term.t Angstrom.t
    val atom : Atom.t Angstrom.t
    val literal : Literal.t Angstrom.t

    val clause : Clause.t Angstrom.t

    val query : Atom.t Angstrom.t

    val db: DB.t Angstrom.t
  end
end

module Make(Signify: Dmc_rdf_signify.S) =  struct

  module D = Datalog_top_down.Make(struct

      (* The datalog implementation makes no differentiation between constants and symbols.
       * This does not make so much sense for us as constants are RDF terms but
       * its hard to make new RDF terms for symbols.
       *
       * Instead we use a simple type to differentiate between terms and symbols.
       *
       * Use polymorphic variant types as we do not want to expose this module anywhere.
       *  *)
      type t =
        [ `Term of Rdf.Term.t
        | `Symbol of string
        ]

      let equal a b =
        match a,b with
        | `Term a_term, `Term b_term -> Rdf.Term.equal a_term b_term
        | `Symbol a_symbol, `Symbol b_symbol -> String.equal a_symbol b_symbol
        | _ -> false

      let hash = Hashtbl.hash

      let to_cbor = function
        | `Term term ->
          Cbor.Array [Cbor.TextString "t"; Rdf_cbor.cbor_of_term term]
        | `Symbol symbol ->
          Cbor.Array [Cbor.TextString "s"; Cbor.TextString symbol]

      let to_string a =
        Cbor.encode @@ to_cbor a

      let parser_symbol =
        let open Angstrom in
        let open Cbor.Parser in
        tuple (any_text_string >>=
               (fun s -> if String.equal s "s" then return () else fail "expecting an s"))
          any_text_string
        >>| (fun (_,s) -> s)

      let parser_term =
        let open Angstrom in
        let open Cbor.Parser in
        tuple (any_text_string >>=
               (fun s -> if String.equal s "t" then return () else fail "expecting an t"))
          Rdf_cbor.Parser.term
        >>| (fun (_,t) -> t)

      let parser_t =
        Angstrom.(choice [parser_term  >>| (fun t -> `Term t);
                          parser_symbol >>| (fun s -> `Symbol s)])

      let of_string s =
        Angstrom.parse_string ~consume:Angstrom.Consume.All parser_t s
        |> Result.get_ok

      let query = `Symbol "?query"
    end)

  module Term = struct
    type t = D.T.t

    let variable = D.T.mk_var
    let constant term =
      D.T.mk_const @@ `Term term

    let constant_value  = function
      | D.T.Apply (`Term term, [||]) ->
        Some term
      | _ -> None

    let equal = D.T.eq

    let rec pp ppf = function
      | D.T.Var var_num ->
        Fmt.pf ppf "?%d" var_num
      | D.T.Apply (`Term term, [||]) ->
        Fmt.pf ppf "%a" Rdf.Term.pp term
      | D.T.Apply (`Symbol predicate, [||]) ->
        Fmt.pf ppf "%s" predicate
      | D.T.Apply (`Symbol predicate, terms) ->
        Fmt.pf ppf "@[<2>%s(%a)@]"
          predicate
          Fmt.(array ~sep:comma pp)
          terms
      | D.T.Apply (`Term predicate, terms) ->
        Fmt.pf ppf "@[<2>%a(%a)@]"
          (* A RDF term should not be in predicate position.... *)
          Rdf.Term.pp predicate
          Fmt.(array ~sep:comma pp)
          terms
  end

  type predicate = string

  module Atom = struct
    type t = D.T.t

    let make symbol terms =
      D.T.mk_apply_l (`Symbol symbol)
        terms

    (* Note this is not type safe as ccube/datalog supports recursive terms (e.g. p(q(x), s(t(n))).
     * We provide a simpler interface that does not support recursive terms. Here this abstraction breaks ...
     *  *)

    let predicate = function
      | D.T.Apply (`Symbol predicate, _) ->
        predicate
      | _ ->
        "There goes our type safety..."

    let terms = function
      | D.T.Apply (_, terms) ->
        Array.to_list terms
      | _ ->
        []

    let equal = D.T.eq

    let triple (t:Triple.t) =
      make "graph" [
        t.subject |> Triple.Subject.to_term |> Term.constant;
        t.predicate |> Triple.Predicate.to_term |> Term.constant;
        t.object' |> Triple.Object.to_term |> Term.constant;
      ]

    let pp = Term.pp
  end

  module Literal = struct
    type t = D.Lit.t

    let make_positive atom =
      D.Lit.mk_pos atom

    let make_negative atom =
      D.Lit.mk_neg atom

    let equal = D.Lit.eq

    let pp ppf = function
      | D.Lit.LitPos atom ->
        Atom.pp ppf atom
      | D.Lit.LitNeg atom ->
        Fmt.pf ppf "@[<4>not %a@]"
          Atom.pp atom
      | D.Lit.LitAggr _ ->
        Fmt.pf ppf "How did you do that??"

  end

  module Clause = struct
    type t = D.C.t

    let make = D.C.mk_clause

    let make_fact = D.C.mk_fact

    let equal = D.C.eq

    let pp ppf (clause:t) =
      match clause.body with
      | [] -> Fmt.pf ppf "@[%a.@]"
                Atom.pp clause.head
      | body -> Fmt.pf ppf "@[%a@ :-@%a.@]"
                  Atom.pp clause.head
                  Fmt.(list ~sep:comma Literal.pp) body
  end

  module DB = struct
    type t = D.DB.t

    let _ed25519Verify_interpreter atom =
      match atom with
      (* TODO the order of terms is taken from the DMC spec. It does not make much
         sense and should be reorderd to signature, public_key, message.*)
      | D.T.Apply (_, [|
          D.T.Apply (`Term signature, [||]);
          D.T.Apply (`Term message, [||]);
          D.T.Apply (`Term public_key, [||])|]) ->
        (match Option.bind (Rdf.Term.to_iri public_key) Signify.PublicKey.of_iri with
         | Some public_key ->
           if Signify.verify ~signature ~public_key message then
             [ D.C.mk_fact atom ]
           else
             []
         | _ -> [])
      | _ ->
        []


    let _containerDefinition_interpreter atom =
      let of_eris_urn urn =
        Rdf.Term.to_iri urn
        |> Option.map (fun urn ->
            "dmc:" ^ (Str.string_after (Rdf.Iri.to_string urn) 11)
            |> Rdf.Iri.of_string
            |> Rdf.Term.of_iri)
      in
      let to_eris_urn urn =
        Rdf.Term.to_iri urn
        |> Option.map (fun urn ->
            "urn:erisx2:" ^ (Str.string_after (Rdf.Iri.to_string urn) 4)
            |> Rdf.Iri.of_string
            |> Rdf.Term.of_iri)
      in

      match atom with

      | D.T.Apply (`Symbol "containerDefinition", [|
          D.T.Apply (`Term container_id, [||]);
          D.T.Var _ |]) ->
        container_id
        |> to_eris_urn
        |> Option.map (fun eris_urn ->
            [Atom.make "containerDefinition"
               [(container_id |> Term.constant);
                (eris_urn |> Term.constant)
               ]
             |> D.C.mk_fact
            ]
          )
        |> Option.value ~default:[]

      | D.T.Apply (`Symbol "containerDefinition", [|
          D.T.Var _;
          D.T.Apply (`Term eris_urn, [||])|]) ->
        eris_urn
        |> of_eris_urn
        |> Option.map (fun container_id ->
            [Atom.make "containerDefinition"
               [(container_id |> Term.constant);
                (eris_urn |> Term.constant)
               ]
             |> D.C.mk_fact
            ]
          )
        |> Option.value ~default:[]

      | D.T.Apply (`Symbol "containerDefinition", [|
          D.T.Apply (`Term container_id, [||]);
          D.T.Apply (`Term eris_urn, [||])|]) ->
        if container_id
           |> to_eris_urn
           |> Option.map (fun urn -> Rdf.Term.equal urn eris_urn)
           |> Option.value ~default:false
        then
          [atom
           |> D.C.mk_fact
          ]
        else
          []

      | _ ->
        []

    let create () =
      let db = D.DB.create () in
      (* D.set_debug true; *)
      D.DB.interpret db (`Symbol "ed25519Verify") _ed25519Verify_interpreter;
      D.DB.interpret db (`Symbol "containerDefinition") _containerDefinition_interpreter;
      db

    let add_clause = D.DB.add_clause

    let add_triple theory triple =
      triple
      |> Atom.triple
      |> Clause.make_fact
      |> add_clause theory

    type query = Atom.t

    let prove db query =
      D.ask db query
  end


  module Parser = struct
    open Angstrom

    let whitespace =
      many @@ choice [string " "; string "\n"; string "\t"]
      >>| ignore

    let xsd_string =
      char '"'
      *> take_while (fun c -> not @@ Char.equal '"' c)
      <* char '"'
      >>| (fun value -> Rdf.Literal.make value (Rdf.Namespace.xsd "string"))

    let iri =
      char '<'
      *> take_while (fun c -> not @@ Char.equal '>' c)
      <* char '>'
      >>| Rdf.Iri.of_string

    let constant =
      choice [
        xsd_string >>| Rdf.Term.of_literal;
        iri >>| Rdf.Term.of_iri
      ]

    let symbol =
      take_while (fun c ->
          let code = Char.code c in
          (* ASCII upper case *)
          (65 <= code && code <= 90)
          ||
          (* ASCII lower case *)
          (97 <= code && code <= 122)
          ||
          (* ASCII numbers  *)
          (48 <= code && code <= 57))

    let number =
      take_while1
        (fun c ->
           let code = Char.code c in
           48 <= code && code <= 57)
      >>| int_of_string

    let variable =
      char '?' *> number

    let term =
      choice [
        constant >>| Term.constant;
        variable >>| Term.variable]

    let comma =
      whitespace
      *> char ','
      <* whitespace

    let atom =
      Atom.make
      <$> symbol
      <* char '('
      <* whitespace
      <*> (sep_by1 comma term)
      <* whitespace
      <* char ')'

    let literal =
      choice [
        string "not"
        *> whitespace
        *> atom
        >>| Literal.make_negative;
        atom >>| Literal.make_positive
      ]

    let fact =
      Clause.make_fact
      <$> atom
      <* whitespace
      <* char '.'

    let rule =
      Clause.make
      <$> atom
      <* whitespace
      <* string ":-"
      <* whitespace
      <*> (sep_by1 comma literal)
      <* whitespace
      <* char '.'

    let clause = choice [ fact; rule ]

    let query = atom <* char '?'

    let db =
      (fun clauses ->
         let db = DB.create () in
         D.DB.add_clauses db clauses;
         db)
      <$> (whitespace
           *> sep_by whitespace clause
           <* whitespace)

  end

end
