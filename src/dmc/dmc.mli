(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Distributed Mutable Containers (DMC)

    An implementation of DMC using {!Irmin.S Irmin stores}.

    @see <http://purl.org/dmc> DMC specification

*)

(** {1 DMC stores} *)

(** A DMC store holds the local state of multiple replicas of DMC containers.

    A DMC store is a wrapper around an {!Irmin.S Irmin store}. It defines a
    structure of the store (where to store what) and functions to access and modify
    the store that correspond to this structure. *)


module type S = sig
  (** DMC stores *)

  type t
  (** The type for DMC stores. This is the same as {!type:Irmin.S.t}. *)

  (**/**)

  (* These are internal APIs that are not displayed in the documentation. *)

  module Crypto : Dmc_crypto.S

  (**/**)

  (** {1 The lower-levels} *)

  (** The following modules provide access the the lower-levels of a DMC store. These are useful when implementing transports or interfaces to DMC containers.

      For a higher-level access to DMC containers, see the {!module:Set} and {!module:Register} modules.*)

  (** {2 Tree} *)

  (** All functions for accessing and mutating content in the store work on
      {!module:Irmin.S.Tree trees}. *)

  (** A few helper functions are provided for easier access. *)

  type tree
  (** The type for store trees.  *)

  val get_tree : t -> tree Lwt.t
  (** [get_tree store] returns the (root) tree of the store. This is the same as
      {!val:Irmin.S.tree}. *)

  val commit_tree : t -> ?info:Irmin.Info.t -> tree -> unit Lwt.t
  (** [commit_tree store ~info tree] atomically commits [tree] to [store]. This
      is a wrapper around {!val:Irmin.S.with_tree}. *)

  (** {2 Content-addressable RDF} *)

  (** DMC uses content-addressed RDF. This includes a grouping of RDF statements
      (triples) into content-addrssable groups called Fragment Graphs.

      @see <http://purl.org/ca-rdf> Content-addressable RDF *)

  module FragmentGraph : Rdf_fragment_graph.S

  (** {2 Block storage and ERIS} *)

  (** DMC uses the ERIS encoding for storing content. The {!module:Blocks} module provides
      access to blocks of ERIS encoded data whereas the {!module:ERIS} provides
      functions for decoding ERIS encoded content (see {!module:Objects} for
      functions for encoding content).

      @see <http://purl.org/eris> ERIS specification *)

  module Blocks : sig
    (** Blocks *)

    type reference = string
    (** Reference to a block (the Blake2b hash of the block). *)

    val add : reference -> string -> tree -> tree Lwt.t
    (** [add reference content tree] adds a block to [tree]. *)

    val add_list: (reference * string) list -> tree -> tree Lwt.t
    (** [add_list blocks tree] add a list of blocks to [tree]. *)

    val get : tree -> reference -> string option Lwt.t
    (** [get tree reference] returns the block if it is present. *)

    val to_list : tree -> (reference * string) list Lwt.t
    (** [to_list tree] returns a list of blocks. *)
  end

  module Eris : sig
    (** Access ERIS encoded content in the store.  *)

    type urn

    val of_iri : Rdf.Iri.t -> urn option
    val of_iri_exn : Rdf.Iri.t -> urn

    val to_iri : urn -> Rdf.Iri.t

    val to_cbor : urn -> Cbor.t
    val cbor_parser : urn Angstrom.t

    val pp : urn Fmt.t

    val get : urn -> tree -> string Lwt.t
    (** [get urn tree] decodes the ERIS encoded content with read capability
        [urn] from blocks in the store. *)

    val get_object : urn -> tree -> (FragmentGraph.t, string) Result.t Lwt.t
    (** [get_object urn tree] decodes the ERIS encoded content with read capability
        [urn] as Fragment Graph. *)
  end

  (** {2 RDF Signify} *)

  (** RDF Signify is a RDF vocabulary for cryptographic signatures. DMC uses RDF
      Signify to represent signatures of operations.

      @see <http://purl.org/signify> RDF Signify specification *)

  module Signify : Dmc_rdf_signify.S

  (** {2 Datalog} *)

  (** Datalog is a query language inspired by Logic Programming. The state
      resolution for DMC containers is specified and implemented as Datalog
      queries.  Datalog can also be used for application level queries to the
      data. *)

  module Datalog : Dmc_datalog.S


  (** {2 Container Identifiers} *)

  module ContainerIdentifier: sig
    type t

    val to_iri : t -> Rdf.Iri.t
    val of_iri : Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val of_eris_urn : Eris.urn -> t
    val to_eris_urn : t -> Eris.urn

    val pp : t Fmt.t
  end


  (** {1 Replica State} *)

  (** A replica is an instance of a DMC container with local state.

      The following modules provide access to the local state of a replica. This
      is useful for imlementing transports. *)

  val replicas: tree -> ContainerIdentifier.t list Lwt.t
  (** [replicas tree] returns a list of replicas in the store. *)

  (** {2 Block references and Objects} *)

  module BlockReferences : sig
    val add : ContainerIdentifier.t -> string -> tree -> tree Lwt.t

    val to_list : ContainerIdentifier.t -> tree -> string list Lwt.t
  end

  module Objects : sig
    (** TODO this module needs more documentaiton *)

    val add_reference : ContainerIdentifier.t -> Eris.urn -> tree -> tree Lwt.t
    (** [add_reference continer_id reference tree] adds [reference] to the list
        of replica objects. You must manually make sure that necessary block
        references to decode content from [reference] are added to replica block
        references. Use {!add_binary} or {!add} for more convenience. *)

    val add_binary : ContainerIdentifier.t -> string -> tree -> (Eris.urn * tree) Lwt.t
    (** [add_binary container_id content tree] encodes [content], stores blocks
        and block references and adds the object reference to list of relpica
        objects. Returns the ERIS URN of the encoded content. This is useful for
        adding binary content to a container.*)

    val add : ContainerIdentifier.t -> FragmentGraph.t -> tree -> (Eris.urn * tree) Lwt.t
    (** [add container_id object tree] encodes [object], stores blocks, adds
        blocks to block references and adds reference to object to replica object
        lists. This is useful for adding a fragment graph to the container. *)

    val to_list : ContainerIdentifier.t -> tree -> Eris.urn list Lwt.t

    val to_triples : ContainerIdentifier.t -> tree -> Rdf.Triple.t list Lwt.t

    val to_datalog_db : ContainerIdentifier.t -> tree -> Datalog.DB.t Lwt.t
  end

  (** {2 Authorized Keys}  *)

  module AuthorizedKeys : sig

    val authorize : ContainerIdentifier.t -> root_secret_key:Signify.SecretKey.t -> Signify.PublicKey.t -> tree -> (Eris.urn * tree) Lwt.t
    (** [authorize container_id ~root_secret_key public_key tree] adds
        [public_key] to the list of authorized keys for container with
        identifier [container_id]. The root secret key of the container must be
        provided with [root_secret_key] to sign the operation.*)

    (* val revoke : ContainerIdentifier.t -> root_secret_key:Signify.SecretKey.t -> Signify.PublicKey.t -> tree -> (Eris.urn * tree) Lwt.t *)
    (* (\** [revoke container_id ~root_secret_key public_key tree] revokes *)
    (*     authorization of [public_key] to mutate container with identifier *)
    (*     [container_id]. The root secret key of the container must be provided *)
    (*     with [root_secret_key] to sign the opreation. *\) *)

    val to_list : ContainerIdentifier.t -> tree -> Signify.PublicKey.t list Lwt.t
    (** [to_list container_id tree] returns a list of known authorized keys for
        the container. *)
  end

  (** {1 Containers} *)

  (** The modules {!module:Set} and {!module:Register} provide an interface for
      accessing and mutating DMC containers.

      Use these modules for applications that use DMC containers and do not care about underlying storage.*)

  (** {2 Set}  *)

  (** Sets are unordered collections that hold distinct members. *)

  (** DMC Sets are based on Observed-Remove Sets (OR-Sets). This means that when
      removing an element `x` from the set we need to provide the reference to the
      operation that added the element `x`. *)

  module Set : sig
    (** DMC set *)

    val initialize : ?secret_key:Signify.SecretKey.t -> tree -> (ContainerIdentifier.t * Signify.SecretKey.t * tree) Lwt.t
    (** [initialize ~secret_key tree] creates a new set definition using
        [secret_key] and add it to [tree].

        If no [secret_key] is provided a new key will be generated.

        @return The container identifier, the secret key and the tree with the initialized container added.*)

    val add_ref : ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> element:Eris.urn -> tree -> (Eris.urn * tree) Lwt.t
    (** [add_ref container_id ~secret_key ~element tree] adds reference [element] to the
        set using [secret_key] to sign the add operation.

        @return The ERIS urn of the operation and the tree with operation and signature added.*)

    val add_binary : ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> content:string -> tree -> (Eris.urn * Eris.urn * tree) Lwt.t
    (** [add_binary container_id ~secret_key ~content tree] adds [content] to the set using [secret_key] to sign the add operation.

        @return The ERIS urn of the added content, of the operation and the tree.*)

    val remove: ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> operation:Eris.urn -> tree -> (Eris.urn * tree) Lwt.t
    (** [remove container_id ~secret_key ~operation tree] remove the elements that were added to the
        set with [operation] using [secret_key] to sign the remove operation.

        @return The ERIS urn of the operation and the tree with operation and signature added.*)

    val members : ContainerIdentifier.t -> tree -> Eris.urn list Lwt.t
    (** [members container_id tree] returns a list of members in the Set. *)

    val resolve : ContainerIdentifier.t -> ?deep:bool -> tree -> Rdf.Graph.t Lwt.t
    (** [resolve container_id ~deep tree] resolves the state of the set with
        identifier [container_id]. If [container_id] is not a set or not locally
        known the empty graph will be returned.*)

  end

  (** {2 Register} *)

  (** A register is a data structure that holds at most a single element. *)

  (** TODO: this is a placeholder, the Register module is not yet implemented. *)

  module Register : sig
  end

end

module Make
    (Crypto : Dmc_crypto.S)
    (Store : Irmin.S with type contents = string and type key = string list):
  S with type t = Store.t and type tree = Store.tree
(** Constructor for a DMC store. *)
