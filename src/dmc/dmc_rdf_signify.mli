(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type S = sig
  (** RDF Signify

      A RDF vocabulary for cryptographically signing content-addressed content
      using the Ed25519 algorithm. *)

  (** @see <http://purl.org/signify> RDF Signify specification *)


  exception Invalid_key
  (** The exception raised when a key can not be decoded from the given
      IRI. *)

  module PublicKey: sig

    type t

    val of_iri: Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val to_iri: t -> Rdf.Iri.t

    val pp : t Fmt.t
  end

  module SecretKey : sig

    type t

    val of_iri: Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val to_iri: t -> Rdf.Iri.t

    val pp : t Fmt.t
  end

  module FragmentGraph : Rdf_fragment_graph.S

  module Namespace : sig
    val public_key : FragmentGraph.Predicate.t
    val message : FragmentGraph.Predicate.t
    val signature : FragmentGraph.Object.t
  end

  val generate_secret_key : unit -> SecretKey.t
  (** [generate_secret_key ()] generates a new secret key (using
      {!Crypto.random_string}) and returns it as iri.*)

  val public_key : SecretKey.t -> PublicKey.t
  (** [public_key secret_key] returns the associated public key of [secret_key].*)

  val signature_value : secret_key:SecretKey.t -> Rdf.Iri.t -> Rdf.Literal.t
  (** [signature_exn ~secret_key message] creates a RDF Signify signature of
      [message] using [secret_key].*)

  val signature : secret_key:SecretKey.t -> Rdf.Iri.t -> FragmentGraph.t
  (** [signature_exn ~secret_key message] creates a RDF Signify signature of
      [message] using [secret_key].*)

  val verify: public_key:PublicKey.t -> signature:Rdf.Term.t -> Rdf.Term.t -> bool
  (** [verify ~public_key ~signature message] verifies if [signature] of [message]
      is valid for [public_key].*)
end


module Make(Crypto: Dmc_crypto.S)
    (FragmentGraph: Rdf_fragment_graph.S) : S with module FragmentGraph = FragmentGraph
