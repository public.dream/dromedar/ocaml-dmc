(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Type signature of DMC cryptographic primitives *)

(** Functions that implement cryptographic primitives are not platform
    independent. In order for the DMC library to be portable the cryptographic
    primitives are abstracted in the following type signature. *)

(** See for example {!module:Dmc_crypto_unix} for an implementation on the Unix
    platform. *)

module type S = sig
  (** Cryptographic primitives  *)

  (** {1 Random Generator}  *)

  val random_string : int -> string
  (** [random_string len] returns a string of [len] random bytes. *)

  (** {1 Hashing} *)

  module Blake2b : sig
    (** BLAKE2b is a fast cryptographically secure hash, based on the ideas of
        Chacha20. It is faster than MD5, yet just as secure as SHA-3. *)

    val digest : ?key:string -> ?size:int -> string -> string
    (** [digest ~key ~size msg] return the Blake2b hash of [msg] of size [size]
        bytes (defaults to 64). If [key] is not specified no key is used.*)
  end

  (** {1 Stream Cypher} *)

  module IETF_ChaCha20 : sig
    (** These functions provide an interface for the Chacha20 encryption
        primitive as specified by the IETF in RFC 8439. *)

    val crypt : key:string -> nonce:string -> ?ctr:int -> string -> string
    (** [crypt ~key ~nonce ~ctr data] returns the XOR of the IETF ChaCha20
        applied to [data] using key [key] (32-byte) nonce [nonce] (12-byte). Counter
        [ctr] (defaults to 0) may be used to specify position in ChaCha20 stream.*)
  end

  (** {1 Public-Key Cryptography} *)

  module Ed25519 : sig
    (** Ed25519 public key signatures and verification with SHA-512 as the
        underlying hash function; they are interoperable with other Ed25519
        implementations. *)

    val public_key : secret_key:string -> string
    (** [public_key ~secret_key] returns the public key of the specifed secret key.*)

    val sign: secret_key:string -> ?public_key:string -> string -> string
    (** [sign ~secret_key ~public_key ~message] returns the Ed25519
        cryptographic signature of [message] using [secret_key]. If [public_key] is
        not given it is recomputed, doubling the execution time. *)

    val check: signature:string -> public_key:string -> string -> bool
    (** [check ~signature ~public_key ~message] checks that [signature] of
        [message] is genuine for the secret key of [public_key].

        WARNING: This function does NOT run in constant time. See the Monocypher
        manual for more information. *)
  end
end
