(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt.Infix
open Lwt.Syntax

module Iri = Rdf.Iri

let src = Logs.Src.create "dmc"
module Log = (val Logs.src_log src : Logs.LOG)

module type S = sig
  (** DMC stores *)

  type t
  (** The type for DMC stores. This is the same as {!type:Irmin.S.t}. *)

  (**/**)

  (* These are internal APIs that are not displayed in the documentation. *)

  module Crypto : Dmc_crypto.S

  (**/**)

  (** {1 The lower-levels} *)

  (** The following modules provide access the the lower-levels of a DMC store. These are useful when implementing transports or interfaces to DMC containers.

      For a higher-level access to DMC containers, see the {!module:Set} and (!module:Register) modules.*)

  (** {2 Tree} *)

  (** All functions for accessing and mutating content in the store work on
      {!module:Irmin.S.Tree trees}. *)

  (** A few helper functions are provided for easier access. *)

  type tree
  (** The type for store trees.  *)

  val get_tree : t -> tree Lwt.t
  (** [get_tree store] returns the (root) tree of the store. This is the same as
      {!val:Irmin.S.tree}. *)

  val commit_tree : t -> ?info:Irmin.Info.t -> tree -> unit Lwt.t
  (** [commit_tree store ~info tree] atomically commits [tree] to [store]. This
      is a wrapper around {!val:Irmin.S.with_tree}. *)

  (** {2 Content-addressable RDF} *)

  (** DMC uses content-addressed RDF. This includes a grouping of RDF statements
      (triples) into content-addrssable groups called Fragment Graphs.

      @see <http://purl.org/ca-rdf> Content-addressable RDF *)

  module FragmentGraph : Rdf_fragment_graph.S

  (** {2 Block storage and ERIS} *)

  (** DMC uses the ERIS encoding for storing content. The {!module:Blocks} module provides
      access to blocks of ERIS encoded data whereas the {!module:ERIS} provides
      functions for decoding ERIS encoded content (see {!module:Objects} for
      functions for encoding content).

      @see <http://purl.org/eris> ERIS specification *)

  module Blocks : sig
    (** Blocks *)

    type reference = string
    (** Reference to a block (the Blake2b hash of the block). *)

    val add : reference -> string -> tree -> tree Lwt.t
    (** [add reference content tree] adds a block to [tree]. *)

    val add_list: (reference * string) list -> tree -> tree Lwt.t
    (** [add_list blocks tree] add a list of blocks to [tree]. *)

    val get : tree -> reference -> string option Lwt.t
    (** [get tree reference] returns the block if it is present. *)

    val to_list : tree -> (reference * string) list Lwt.t
    (** [to_list tree] returns a list of blocks. *)
  end

  module Eris : sig
    (** Access ERIS encoded content in the store.  *)

    type urn
    (** Type for a ERIS URN  *)

    val of_iri : Rdf.Iri.t -> urn option
    val of_iri_exn : Rdf.Iri.t -> urn

    val to_iri : urn -> Rdf.Iri.t

    val to_cbor : urn -> Cbor.t
    val cbor_parser : urn Angstrom.t

    val pp : urn Fmt.t

    val get : urn -> tree -> string Lwt.t
    (** [get urn tree] decodes the ERIS encoded content with read capability
        [urn] from blocks in the store. *)

    val get_object : urn -> tree -> (FragmentGraph.t, string) Result.t Lwt.t
    (** [get_object urn tree] decodes the ERIS encoded content with read capability
        [urn] as Fragment Graph. *)
  end

  (** {2 RDF Signify} *)

  (** RDF Signify is a RDF vocabulary for cryptographic signatures. DMC uses RDF
      Signify to represent signatures of operations.

      @see <http://purl.org/signify> RDF Signify specification *)

  module Signify : Dmc_rdf_signify.S

  (** {2 Datalog} *)

  (** Datalog is a query language inspired by Logic Programming. The state
      resolution for DMC containers is specified and implemented as Datalog
      queries.  Datalog can also be used for application level queries to the
      data. *)

  module Datalog : Dmc_datalog.S


  (** {2 Container Identifiers} *)

  module ContainerIdentifier: sig
    type t

    val to_iri : t -> Rdf.Iri.t
    val of_iri : Rdf.Iri.t -> t option
    val of_iri_exn : Rdf.Iri.t -> t

    val of_eris_urn : Eris.urn -> t
    val to_eris_urn : t -> Eris.urn

    val pp : t Fmt.t
  end

  (** {1 Replica State} *)

  (** A replica is an instance of a DMC container with local state.

      The following modules provide access to the local state of a replica. This
      is useful for imlementing transports. *)

  val replicas: tree -> ContainerIdentifier.t list Lwt.t
  (** [replicas tree] returns a list of replicas in the store. *)

  (** {2 Block references and Objects} *)

  module BlockReferences : sig
    val add : ContainerIdentifier.t -> string -> tree -> tree Lwt.t

    val to_list : ContainerIdentifier.t -> tree -> string list Lwt.t
  end

  module Objects : sig
    (** TODO this module needs more documentaiton *)

    val add_reference : ContainerIdentifier.t -> Eris.urn -> tree -> tree Lwt.t
    (** [add_reference continer_id reference tree] adds [reference] to the list
        of replica objects. You must manually make sure that necessary block
        references to decode content from [reference] are added to replica block
        references. Use {!add_binary} or {!add} for more convenience. *)

    val add_binary : ContainerIdentifier.t -> string -> tree -> (Eris.urn * tree) Lwt.t
    (** [add_binary container_id content tree] encodes [content], stores blocks
        and block references and adds the object reference to list of relpica
        objects. Returns the ERIS URN of the encoded content. This is useful for
        adding binary content to a container.*)

    val add : ContainerIdentifier.t -> FragmentGraph.t -> tree -> (Eris.urn * tree) Lwt.t
    (** [add container_id object tree] encodes [object], stores blocks, adds
        blocks to block references and adds reference to object to replica object
        lists. This is useful for adding a fragment graph to the container. *)

    val to_list : ContainerIdentifier.t -> tree -> Eris.urn list Lwt.t

    val to_triples : ContainerIdentifier.t -> tree -> Rdf.Triple.t list Lwt.t

    val to_datalog_db : ContainerIdentifier.t -> tree -> Datalog.DB.t Lwt.t
  end

  (** {2 Authorized Keys}  *)

  module AuthorizedKeys : sig

    val authorize : ContainerIdentifier.t -> root_secret_key:Signify.SecretKey.t -> Signify.PublicKey.t -> tree -> (Eris.urn * tree) Lwt.t
    (** [authorize container_id ~root_secret_key public_key tree] adds
        [public_key] to the list of authorized keys for container with
        identifier [container_id]. The root secret key of the container must be
        provided with [root_secret_key] to sign the operation.*)

    (* val revoke : ContainerIdentifier.t -> root_secret_key:Signify.SecretKey.t -> Signify.PublicKey.t -> tree -> (Eris.urn * tree) Lwt.t *)
    (* (\** [revoke container_id ~root_secret_key public_key tree] revokes *)
    (*     authorization of [public_key] to mutate container with identifier *)
    (*     [container_id]. The root secret key of the container must be provided *)
    (*     with [root_secret_key] to sign the opreation. *\) *)

    val to_list : ContainerIdentifier.t -> tree -> Signify.PublicKey.t list Lwt.t
    (** [to_list container_id tree] returns a list of known authorized keys for
        the container. *)
  end

  (** {1 Containers} *)

  (** The modules {!module:Set} and {!module:Register} provide an interface for
      accessing and mutating DMC containers.

      Use these modules for applications that use DMC containers and do not care about underlying storage.*)

  (** {2 Set}  *)

  (** Sets are unordered collections that hold distinct members. *)

  (** DMC Sets are based on Observed-Remove Sets (OR-Sets). This means that when
      removing an element `x` from the set we need to provide the reference to the
      operation that added the element `x`. *)

  module Set : sig
    (** DMC set *)

    val initialize : ?secret_key:Signify.SecretKey.t -> tree -> (ContainerIdentifier.t * Signify.SecretKey.t * tree) Lwt.t
    (** [initialize ~secret_key tree] creates a new set definition using
        [secret_key] and add it to [tree].

        If no [secret_key] is provided a new key will be generated.

        @return The container identifier, the secret key and the tree with the initialized container added.*)

    val add_ref : ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> element:Eris.urn -> tree -> (Eris.urn * tree) Lwt.t
    (** [add container_id ~secret_key ~element tree] adds the [element] to the
        set using [secret_key] to sign the add operation.

        @return The ERIS urn of the operation and the tree with operation and signature added.*)

    val add_binary : ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> content:string -> tree -> (Eris.urn * Eris.urn * tree) Lwt.t
    (** [add_binary container_id ~secret_key ~content tree] adds [content] to the set using [secret_key] to sign the add operation.

        @returns The ERIS urn of the added content, of the operation and the tree.*)

    val remove: ContainerIdentifier.t -> secret_key:Signify.SecretKey.t -> operation:Eris.urn -> tree -> (Eris.urn * tree) Lwt.t
    (** [remove container_id ~secret_key ~operation tree] remove the elements that were added to the
        set with [operation] using [secret_key] to sign the remove operation.

        @return The ERIS urn of the operation and the tree with operation and signature added.*)

    val members : ContainerIdentifier.t -> tree -> Eris.urn list Lwt.t
    (** [members container_id tree] returns a list of members in the Set. *)

    val resolve : ContainerIdentifier.t -> ?deep:bool -> tree -> Rdf.Graph.t Lwt.t
    (** [resolve container_id ~deep tree] resolves the state of the set with
        identifier [container_id]. If [container_id] is not a set or not locally
        known the empty graph will be returned.*)
  end


  (** {2 Register} *)

  (** A register is a data structure that holds at most a single element. *)

  (** TODO: this is a placeholder, the Register module is not yet implemented. *)

  module Register : sig
  end

end

module Make
    (Crypto : Dmc_crypto.S)
    (Store : Irmin.S with type contents = string and type key = string list) = struct

  type t = Store.t

  let null_convergence_secret =
    String.make 32 (Char.chr 0)

  module FragmentGraph = Rdf_fragment_graph.Make(struct
      let hash s =
        let _dict, _read_capability, urn =
          Eris.encode_urn s ~convergence_secret: null_convergence_secret ~block_size: 1024 in
        Rdf.Iri.of_string urn
    end)

  module Namespace = struct
    (* Helpers or using the DMC namespace *)


    let dmc = Rdf.Namespace.make_namespace "http://purl.org/dmc/ns#"

    (* Directly cast to a Fragment Graph predicate *)
    let dmc_p s =
      dmc s
      |> FragmentGraph.Predicate.of_iri

    (* Directly cast to a Fragment Graph object *)
    let dmc_o s =
      dmc s
      |> FragmentGraph.Object.of_iri

    let container =
      dmc_p "container"
  end

  module Crypto = Crypto

  type tree = Store.tree

  let get_tree store = Store.tree store
  let commit_tree store ?(info=Irmin.Info.empty) tree =
    let* result = Store.with_tree ~info:(fun () -> info) store []
        (fun _old -> Lwt.return @@ Some tree) in
    match result with
    | Ok () ->
      Log.info (fun m -> m "tree commited (message: %s)"
                   (Irmin.Info.message info));
      Lwt.return_unit
    | Error (`Conflict msg) -> Lwt.fail_with msg
    | Error (`Test_was _) -> Lwt.fail_with "This should not happen as we did not use the test and set strategy."
    | Error (`Too_many_retries _n) -> Lwt.fail_with "too many retries"

  (* Helper for getting keys of immediate children *)
  let tree_child_keys tree =
    Store.Tree.fold
      ~depth:(`Eq 1)
      ~node:(fun key _node acc ->
          match key with
          | [k] ->
            k :: acc
            |> Lwt.return
          | _ ->
            Lwt.return acc)
      ~contents:(fun key _contents acc ->
          match key with
          | [k] ->
            k :: acc
            |> Lwt.return
          | _ ->
            Lwt.return acc)
      tree
      []

  module Blocks = struct
    type reference = string

    let add reference block tree =
      Store.Tree.add tree ["blocks"; reference] block

    let add_list blocks tree =
      Lwt_list.fold_left_s
        (fun tree (reference, block) ->
           add reference block tree
        )
        tree
        blocks

    let get tree reference =
      Store.Tree.find tree ["blocks"; reference]
      (* Check integrity of block *)
      >|= (fun block_opt -> Option.bind block_opt (fun block ->
          if String.equal (Crypto.Blake2b.digest ~size:32 block)
              reference
          then
            Some block
          else
            begin
              Log.warn (fun m -> m "integrity check on block failed (block reference: %a)"
                           Fmt.(hex ~w:4 () |> on_string)
                           reference);
              None
            end))

    let to_list tree =
      let* blocks_tree_opt = Store.Tree.find_tree tree ["blocks"] in
      match blocks_tree_opt with
      | None -> Lwt.return []
      | Some blocks_tree ->
        Store.Tree.fold
          ~depth:(`Eq 1)
          ~contents:(fun key block blocks ->
              match key with
              | [reference] ->
                (reference, block) :: blocks
                |> Lwt.return
              | _ ->
                Lwt.return blocks)
          blocks_tree
          []

  end

  module Eris = struct

    include Dmc_eris

    let encode = Eris.encode

    let get urn tree =
      let* blocks = Blocks.to_list tree in
      Eris.decode urn blocks
      |> Lwt.return

    let get_object tree urn =
      let* raw = get tree urn in
      FragmentGraph.of_canonical raw
      |> Lwt.return
  end

  module Signify = Dmc_rdf_signify.Make(Crypto)(FragmentGraph)
  module Datalog = Dmc_datalog.Make(Signify)

  module ContainerIdentifier = struct
    (* Helpers for computing container id from ERIS URN *)

    type t = Eris.urn

    let to_iri id =
      Eris.to_iri id
      |>  (fun iri -> "dmc:" ^ (Str.string_after (Rdf.Iri.to_string iri) 11)
                      |> Rdf.Iri.of_string)

    let of_iri iri =
      iri
      |> (fun iri -> "urn:erisx2:" ^ (Str.string_after (Rdf.Iri.to_string iri) 4)
                     |> Rdf.Iri.of_string)
      |> Eris.of_iri

    let of_iri_exn iri =
      iri
      |> (fun iri -> "urn:erisx2:" ^ (Str.string_after (Rdf.Iri.to_string iri) 4)
                     |> Rdf.Iri.of_string)
      |> Eris.of_iri_exn

    let of_eris_urn urn = urn

    let to_eris_urn id  = id

    let of_definition definition =
      FragmentGraph.base_subject definition
      |> Eris.of_iri_exn
      |> of_eris_urn

    let pp ppf id =
      Fmt.pf ppf "%a"
        Rdf.Iri.pp
        (to_iri id)
  end

  (* Replica state *)

  let replicas tree =
    let* tree_opt = Store.Tree.find_tree tree ["replica"] in
    match tree_opt with
    | None -> Lwt.return_nil
    | Some tree ->
      tree_child_keys tree
      >|= List.map
        (fun s -> ContainerIdentifier.of_iri_exn @@ Iri.of_string s)

  module BlockReferences = struct
    let add id reference tree =
      Store.Tree.add tree
        ["replica";
         Iri.to_string @@ ContainerIdentifier.to_iri id;
         "block_references";
         reference]
        ""
    let add_dict id blocks tree =
      Lwt_list.fold_left_s
        (fun tree (reference, _block) ->
           add id reference tree
        )
        tree
        blocks

    let to_list id tree =
      let* tree_opt = Store.Tree.find_tree tree
          ["replica";
           Iri.to_string @@ ContainerIdentifier.to_iri id;
           "block_references"]
      in
      match tree_opt with
      | None -> Lwt.return []
      | Some tree ->
        tree_child_keys tree
  end

  module Objects = struct

    let src = Logs.Src.create "Dmc.Objects"
    module Log = (val Logs.src_log src : Logs.LOG)

    let add_reference id reference tree =
      Log.debug (fun m -> m "add_reference %a to %a"
                    ContainerIdentifier.pp id
                    Eris.pp reference);
      Store.Tree.add tree
        ["replica";
         Iri.to_string @@ ContainerIdentifier.to_iri id;
         "objects";
         Iri.to_string @@ Eris.to_iri reference;]
        ""

    let add_binary id binary tree =
      (* Encode with ERIS *)
      let blocks, urn =
        Eris.encode
          ~convergence_secret:null_convergence_secret
          ~block_size:1024
          binary
      in
      let* tree =
        tree
        (* add the blocks to the block storage *)
        |> Blocks.add_list blocks
        (* add block references *)
        >>= BlockReferences.add_dict id blocks
        (* add reference to the replica objects *)
        >>= add_reference id urn
      in
      Lwt.return (urn, tree)

    let add id object' tree =
      add_binary id
        (FragmentGraph.canonical object')
        tree

    let to_list id tree =
      let* tree_opt = Store.Tree.find_tree tree
          ["replica";
           Iri.to_string @@ ContainerIdentifier.to_iri id;
           "objects"]
      in
      match tree_opt with
      | None -> Lwt.return []
      | Some tree ->
        tree_child_keys tree
        >|= List.map (fun s -> Eris.of_iri_exn @@ Iri.of_string s)

    let to_triples container_id tree =
      let* tree_opt = Store.Tree.find_tree tree
          ["replica";
           Iri.to_string @@ ContainerIdentifier.to_iri container_id;
           "objects"]
      in
      match tree_opt with
      | None ->
        Lwt.return_nil
      | Some object_tree ->
        Store.Tree.fold
          ~depth:(`Eq 1)
          ~contents:(fun key _ triples ->
              match key with
              | [object_urn] ->
                let* fragment_graph =
                  Eris.get_object (Eris.of_iri_exn @@
                                   Rdf.Iri.of_string object_urn) tree
                  >|= Result.value ~default:FragmentGraph.empty
                in
                (* Add all triples from the fragment graph to the DB *)
                List.append triples
                  (List.of_seq @@ FragmentGraph.to_triples fragment_graph)
                |> Lwt.return
              | _ -> Lwt.return triples)
          object_tree
          []

    (* This is basically the Datalog copied from the DMC specification with some modifications due to technical limitations:
     *
     *  - Variables can only be integers (TODO: allow arbirtary symbols as variables)
     *  - No shortcuts or prefixes for IRIs (e.g. a is <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>)
     *
     * Why is this a string? Because it's hard to construct the AST for such a program by hand.
     *
     *  *)
    let dmc_datalog =
      {datalog|

          signed(?1, ?2) :-
              graph(?3, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://purl.org/signify/ns#Signature>),
              graph(?3, <http://purl.org/signify/ns#message>, ?1),
              graph(?3, <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>, ?4),
              ed25519Verify(?4, ?1, ?2).

          authorizedKey(?1, ?2) :-
              containerDefinition(?1, ?3),
              graph(?3, <http://purl.org/dmc/ns#rootPublicKey>, ?2).

          authorizedKey(?1, ?2) :-
              containerDefinition(?1, ?5),
              graph(?5, <http://purl.org/dmc/ns#rootPublicKey>, ?3),
              graph(?4, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://purl.org/dmc/ns#AuthorizeKey>),
              graph(?4, <http://purl.org/dmc/ns#container>, ?1),
              graph(?4, <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>, ?2),
              signed(?4, ?3).

          authorizedOperation(?1, ?2) :-
            graph(?2, <http://purl.org/dmc/ns#container>, ?1),
            authorizedKey(?1, ?3),
            signed(?2, ?3).

          setOperationRemoved(?1,?2) :-
            graph(?2, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://purl.org/dmc/ns#Add>),
            graph(?2, <http://purl.org/dmc/ns#container>, ?1),
            graph(?3, <http://purl.org/dmc/ns#operation>, ?2),
            graph(?3, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://purl.org/dmc/ns#Remove>),
            authorizedOperation(?1, ?3).

          setMember(?1, ?2) :-
              containerDefinition(?1, ?5),
              graph(?5, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <http://purl.org/dmc/ns#SetDefinition>),
              graph(?3, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>,  <http://purl.org/dmc/ns#Add>),
              graph(?3, <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>, ?2),
              authorizedOperation(?1, ?3),
              not setOperationRemoved(?1, ?3).

        |datalog}

    let create_datalog_db () =
      Angstrom.(parse_string ~consume:Consume.All Datalog.Parser.db dmc_datalog)
      |> Result.get_ok

    let to_datalog_db container_id tree =
      let* triples = to_triples container_id tree in
      List.fold_left
        (fun db triple -> Datalog.DB.add_triple db triple; db)
        (create_datalog_db ())
        triples
      |> Lwt.return

  end

  module AuthorizedKeys = struct

    let authorize container_id ~root_secret_key public_key tree =
      let open FragmentGraph.Namespace in
      let open Namespace in

      (* Create an AuthorizeKey operation *)
      let authorize_key_op = FragmentGraph.(
          empty
          |> add_statement a (dmc_o "AuthorizeKey")
          |> add_statement value (Object.of_iri @@ Signify.PublicKey.to_iri public_key)
          |> add_statement container (Object.of_iri @@
                                      ContainerIdentifier.to_iri container_id))
      in

      (* add the operation to the tree *)
      let* operation, tree = Objects.add container_id authorize_key_op tree in

      (* create a signature of the operation *)
      let signature = Signify.signature ~secret_key:root_secret_key (Eris.to_iri operation) in

      (* add the signature to the tree *)
      let* _signature_urn, tree = Objects.add container_id signature tree in

      (* Return the operation URN and the tree *)
      Lwt.return (operation, tree)


    (* let revoke container_id ~root_secret_key public_key tree = *)
    (*   let open FragmentGraph.Namespace in *)
    (*   let open Namespace in *)

    (*   (\* create an RevokeKey operation *\) *)
    (*   let revoke_key_op = FragmentGraph.( *)
    (*       empty *)
    (*       |> add_statement a (dmc_o "RevokeKey") *)
    (*       |> add_statement value (Object.of_iri @@ Signify.PublicKey.to_iri public_key) *)
    (*       |> add_statement container (Object.of_iri @@ *)
    (*                                   ContainerIdentifier.to_iri container_id)) *)
    (*   in *)

    (*   (\* add the operation to the tree *\) *)
    (*   let* operation, tree = Objects.add container_id revoke_key_op tree in *)

    (*   (\* create a signature of the operation *\) *)
    (*   let signature = Signify.signature ~secret_key:root_secret_key (Eris.to_iri operation) in *)

    (*   (\* add the signature to the tree *\) *)
    (*   let* _signature_urn, tree = Objects.add container_id signature tree in *)

    (*   (\* Return the operation URN and the tree *\) *)
    (*   Lwt.return (operation, tree) *)

    let to_list container_id tree =
      let open Datalog in

      (* create a Datalog DB from replica objects *)
      let* db = Objects.to_datalog_db container_id tree in

      (* formulate a query for the predicate "authorizedKey" *)
      let query = Atom.make "authorizedKey"
          [Term.constant @@ Rdf.Term.of_iri @@
           ContainerIdentifier.to_iri container_id;
           Term.variable 1]
      in

      (* run the query *)
      let results = DB.prove db query in

      (* Get keys from returned ground atoms in results *)
      results
      |> List.filter_map (fun atom ->
          match Atom.terms atom with
          | [_; authorized_key_term] ->
            Option.bind
              (* Get the Datalog terms value. *)
              (Term.constant_value authorized_key_term)
              (fun term ->
                 (* Get the public keys from the RDF IRI *)
                 Option.bind (Rdf.Term.to_iri term)
                   Signify.PublicKey.of_iri)
          | _ -> None)
      |> Lwt.return

  end

  (* Containers *)

  module Set = struct

    let src = Logs.Src.create "Dmc.Objects"
    module Log = (val Logs.src_log src : Logs.LOG)

    let initialize ?(secret_key=Signify.generate_secret_key ()) tree =
      let open FragmentGraph.Namespace in
      let open Namespace in
      Log.info (fun m -> m "initializing a new set");
      let root_public_key = Signify.public_key secret_key in
      (* Create a simple definition TODO allow more statements to be added *)
      let definition =
        FragmentGraph.(
          empty
          |> add_statement a (dmc_o "SetDefinition")
          |> add_statement
            (dmc_p "rootPublicKey")
            (FragmentGraph.Object.of_iri @@
             Signify.PublicKey.to_iri root_public_key))
      in
      (* Compute the identifier of the container *)
      let container_id = ContainerIdentifier.of_definition definition in
      (* add to tree *)
      let* _definition_urn, tree =
        tree
        |> Objects.add container_id definition
      in
      (container_id, secret_key, tree)
      |> Lwt.return

    let add_ref container_id ~secret_key ~element tree =
      let open FragmentGraph.Namespace in
      let open Namespace in
      (* Create an Add operation *)
      let add_op = FragmentGraph.(
          empty
          |> add_statement a (dmc_o "Add")
          |> add_statement value (Object.of_iri @@ Eris.to_iri element)
          |> add_statement container (Object.of_iri @@
                                      ContainerIdentifier.to_iri container_id))
      in
      (* add the operation to tree *)
      let* operation, tree =
        tree
        |> Objects.add container_id add_op
      in
      (* Create a signature of the operation *)
      let signature = Signify.signature ~secret_key (Eris.to_iri operation) in
      (* Add the signature to the tree *)
      let* _signature_urn, tree =
        tree
        |> Objects.add container_id signature
      in
      (operation, tree)
      |> Lwt.return

    let add_binary container_id ~secret_key ~content tree =
      let open FragmentGraph.Namespace in
      let open Namespace in

      (* Add the binary content to the container *)
      let* content_urn, tree =
        Objects.add_binary container_id content tree
      in

      (* Create an operation to add the content *)
      let add_op = FragmentGraph.(
          empty
          |> add_statement a (dmc_o "Add")
          |> add_statement value
            (Object.of_iri @@ Eris.to_iri content_urn)
          |> add_statement container
            (Object.of_iri @@ ContainerIdentifier.to_iri container_id))
      in

      (* add operation to container *)
      let* add_op_urn, tree = Objects.add container_id add_op tree in

      (* Create a signature of the add operatoin *)
      let signature = Signify.signature ~secret_key
          (Eris.to_iri add_op_urn) in

      (* Add the signature to the tree *)
      let* _signature_urn, tree =
        tree
        |> Objects.add container_id signature
      in

      (content_urn, add_op_urn, tree)
      |> Lwt.return

    let remove container_id ~secret_key ~operation tree =
      let open FragmentGraph.Namespace in
      let open Namespace in
      let remove_op = FragmentGraph.(
          empty
          |> add_statement a (dmc_o "Remove")
          |> add_statement container
            (Object.of_iri @@ ContainerIdentifier.to_iri container_id)
          |> add_statement (dmc_p "operation")
            (Object.of_iri @@ Eris.to_iri operation)
        )
      in
      let* operation, tree =
        tree
        |> Objects.add container_id remove_op
      in

      (* Create a signature of the operation *)
      let signature = Signify.signature ~secret_key
          (Eris.to_iri operation) in

      let* _signature_urn, tree =
        tree
        |> Objects.add container_id signature
      in
      (operation, tree)
      |> Lwt.return

    let members container_id tree =
      let open Datalog in
      let* db = Objects.to_datalog_db container_id tree in
      let query = Atom.make "setMember"
          [Term.constant @@ Rdf.Term.of_iri @@
           ContainerIdentifier.to_iri container_id;
           Term.variable 1]
      in
      Log.debug (fun m -> m "members: running Datalog query: %a" Atom.pp query);
      let results = DB.prove db query in
      Log.debug (fun m -> m "members: Datalog query return %d atoms" (List.length results));
      results
      |> List.filter_map (fun atom ->
          match Atom.terms atom with
          | [_; member_term] ->
            Option.bind
              (member_term |> Term.constant_value)
              (fun term ->
                 Option.bind (Rdf.Term.to_iri term)
                   Eris.of_iri)
          | _ -> None)
      |> Lwt.return

    let resolve container_id ?(deep=false) tree =
      let* container_definition_res =
        Eris.get_object
          (ContainerIdentifier.to_eris_urn container_id)
          tree
      in
      match container_definition_res with
      | Ok definition ->
        (* Create a RDF Graph containing the container definiton and member elements. *)
        let* members = members container_id tree in
        let container_iri = ContainerIdentifier.to_iri container_id in
        Rdf.Graph.empty
        (* Add all statements from definition with container_id as subject. *)
        |> Rdf.Graph.add_seq (definition
                              |> FragmentGraph.statements
                              |> Seq.map (fun (p,o) ->
                                  Rdf.Triple.make
                                    (Rdf.Triple.Subject.of_iri @@ container_iri)
                                    (FragmentGraph.Predicate.expand ~base_subject:container_iri p)
                                    (FragmentGraph.Object.expand ~base_subject:container_iri o))
                             )
        (* Add all the fragment statments from definition with conatiner_id as subject. *)
        |> Rdf.Graph.add_seq (definition
                              |> FragmentGraph.fragment_statements
                              |> Seq.map (fun (f,p,o) ->
                                  Rdf.Triple.make
                                    (Rdf.Triple.Subject.of_iri @@ Rdf.Iri.with_fragment container_iri (Some f))
                                    (FragmentGraph.Predicate.expand ~base_subject:container_iri p)
                                    (FragmentGraph.Object.expand ~base_subject:container_iri o)))
        (* Remove the SetDefinition type definition *)
        |> Rdf.Graph.remove Rdf.Triple.(make
                                          (Subject.of_iri @@
                                           ContainerIdentifier.to_iri container_id)
                                          (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
                                          (Object.of_iri @@ Namespace.dmc "SetDefinition"))
        (* Add the type declaration "dmc:Set" *)
        |> Rdf.Graph.add Rdf.Triple.(make
                                       (Subject.of_iri @@ ContainerIdentifier.to_iri container_id)
                                       (Predicate.of_iri @@ Rdf.Namespace.rdf "type")
                                       (Object.of_iri @@ Namespace.dmc "Set"))
        (* Add members with member predicate *)
        |> Rdf.Graph.add_seq (members
                              |> List.map (fun eris_urn ->
                                  Rdf.Triple.(make
                                                (Subject.of_iri @@ ContainerIdentifier.to_iri container_id)
                                                (Predicate.of_iri @@ Namespace.dmc "member")
                                                (Object.of_iri @@ Eris.to_iri eris_urn)))
                              |> List.to_seq)
        (* Get member elements themselves *)
        |> (fun graph ->
            if deep then
              Lwt_list.fold_left_s (fun graph element_urn ->
                  let* element_res = Eris.get_object element_urn tree in
                  match element_res with
                  | Ok element ->
                    graph
                    |> Rdf.Graph.add_seq (FragmentGraph.to_triples element)
                    |> Lwt.return

                  | Error _ ->
                    graph
                    |> Lwt.return)
                graph
                members
            else
              Lwt.return graph)

      | _ ->
        Rdf.Graph.empty
        |> Lwt.return

  end

  module Register = struct
  end

end
