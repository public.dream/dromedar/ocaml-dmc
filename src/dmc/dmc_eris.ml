(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

exception Invalid_read_capability


type urn = Eris.ReadCapability.t

let make block_size level root_reference root_key : urn =
  {block_size; level; root_reference; root_key}

let to_binary (read_capability:urn) =
  let b = Buffer.create 66 in
  let initial_byte = if read_capability.block_size = 1024 then
      10
    else if read_capability.block_size = 32768 then
      15
    else
      invalid_arg "Invalid block size"
  in
  Buffer.(
    (* initial byte with block size *)
    add_uint8 b initial_byte;
    (* level *)
    add_uint8 b read_capability.level;
    (* root reference *)
    add_string b read_capability.root_reference;
    (* root key *)
    add_string b read_capability.root_key
  );
  Buffer.to_bytes b
  |> Bytes.to_string

let binary_parser =
  let open Angstrom in
  make
  <$> (any_uint8 >>= (fun initial_byte ->
      if initial_byte = 10 then
        return 1024
      else if initial_byte = 15 then
        return 32768
      else
        fail ("unexpected initial byte " ^ (Int.to_string initial_byte))))
  <*> any_uint8
  <*> Angstrom.take 32
  <*> Angstrom.take 32

let of_binary s =
  Angstrom.parse_string ~consume:Angstrom.Consume.All
    binary_parser s

let to_string read_capability =
  let binary = to_binary read_capability in
  "urn:erisx2:" ^ (Base32.encode_string ~pad:false binary)

let string_parser =
  let open Angstrom in
  string "urn:erisx2:"
  *> take 106
  >>= (fun encoded ->
      match Base32.decode encoded with
      | Ok binary -> return binary
      | Error (`Msg msg) -> fail msg
    )
  >>= (fun binary ->
      match of_binary binary with
      | Ok read_cap -> return read_cap
      | Error msg -> fail msg
    )

let of_string s =
  Angstrom.parse_string ~consume:Angstrom.Consume.All
    string_parser s

let to_cbor read_capability =
  Cbor.Tag (Z.(~$276), Cbor.ByteString (to_binary read_capability))

let cbor_parser =
  let open Angstrom in
  let open Cbor.Parser in
  tag Z.(~$276)
    (any_byte_string
     (* TODO there must be a better way of doing this then restarting another
        Angstrom parser. But I don't see it...seems tricky that size of binary
        to be read is bounded by CBOR.*)
     >>= (fun s -> match of_binary s with
         | Ok read_cap -> return read_cap
         | Error msg -> fail msg))


let of_iri iri =
  match of_string @@ Rdf.Iri.to_string iri with
  | Ok read_cap -> Some read_cap
  | Error _ -> None

let of_iri_exn iri =
  match of_iri iri with
  | Some urn -> urn
  | None -> raise Invalid_read_capability

let to_iri read_cap =
  to_string read_cap
  |> Rdf.Iri.of_string

let pp ppf urn =
  Fmt.pf ppf "%a"
    Rdf.Iri.pp
    (to_iri urn)
