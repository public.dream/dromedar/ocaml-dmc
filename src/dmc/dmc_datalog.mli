(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type S = sig
  module Term : sig
    type t
    val variable : int -> t
    val constant: Rdf.Term.t -> t

    val constant_value : t -> Rdf.Term.t option

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  type predicate = string

  module Atom: sig
    type t
    val make : predicate -> Term.t list -> t

    val predicate : t -> predicate
    val terms : t -> Term.t list

    val equal : t -> t -> bool

    val triple : Rdf.Triple.t -> t

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module Literal : sig
    type t

    val make_positive : Atom.t -> t
    val make_negative : Atom.t -> t

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module Clause : sig
    type t
    val make : Atom.t -> Literal.t list -> t
    val make_fact : Atom.t -> t

    (* val head : t -> Literal.t
     * val body : t -> Literal.t list *)

    val equal : t -> t -> bool

    val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  end

  module DB: sig
    type t
    val create : unit -> t

    val add_clause: t -> Clause.t -> unit
    val add_triple : t -> Rdf.Triple.t -> unit

    type query = Atom.t
    val prove : t -> query -> Atom.t list
  end

  module Parser : sig

    (** Parsers for Datalog programs. *)

    val xsd_string : Rdf.Literal.t Angstrom.t
    val iri : Rdf.Iri.t Angstrom.t

    val constant : Rdf.Term.t Angstrom.t
    val variable : int Angstrom.t

    val term : Term.t Angstrom.t
    val atom : Atom.t Angstrom.t
    val literal : Literal.t Angstrom.t

    val clause : Clause.t Angstrom.t

    val query : Atom.t Angstrom.t

    val db: DB.t Angstrom.t
  end
end

module Make(Signify: Dmc_rdf_signify.S) : S
