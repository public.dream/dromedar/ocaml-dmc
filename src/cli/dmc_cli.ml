(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* DMC Unix Command Line Interface Tool
 *
 * Provides a CLI interface to DMC containers with a persistent storage.
 *
 *  *)

open Lwt.Infix
open Lwt.Syntax

(* Setup logging *)
let src = Logs.Src.create "dmc_cli"
module Log = (val Logs.src_log src : Logs.LOG)


(* Setup the DMC store *)

(* The Irmin backend *)
module Backend = Irmin_pack.KV(
  struct
    (* TODO figure out what these parameters mean *)
    let entries = 2
    let stable_hash = 3
  end)(Irmin.Contents.String)

(* The DMC store *)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Backend)


(* Input/Output helpers *)

type rdf_format =
  | Turtle
  | Json
  | Debug

module Input = struct

  let rdf_format_t =
    let open Cmdliner in
    let doc = "Set the input format (valid values are: turtle and json)." in
    let env = Arg.env_var "DMC_INPUT_FORMAT" in
    let format = Arg.enum ["turtle", Turtle; "json", Json;] in
    Arg.(value & opt format Turtle & info ["input-format"; "i"] ~env ~docv:"INPUT_FORMAT" ~doc)

  let filename_t =
    let open Cmdliner in
    let doc = "Read input from given file. If no file is specified input is read from stdin." in
    Arg.(value & opt (some string) None & info ["file"; "f"] ~docv:"FILE" ~doc)

  let read_raw filename_opt =
    (match filename_opt with
     | Some filename ->
       Log.app (fun m -> m "Reading from %s." filename);
       Lwt_unix.(openfile filename [O_RDONLY] 0)
       >|= (fun fd -> Lwt_io.of_fd ~mode:Lwt_io.Input fd)
     | None ->
       Log.app (fun m -> m "Reading from standard input (ctrl-d to terminate).");
       Lwt_io.stdin |> Lwt.return
    )
    >>= Lwt_io.read
end


module Output = struct

  let rdf_format_t =
    let open Cmdliner in
    let doc = "Set the RDF output format (valid values are: turtle, json and debug)." in
    let env = Arg.env_var "DMC_OUTPUT_FORMAT" in
    let format = Arg.enum ["turtle", Turtle; "json", Json; "debug", Debug] in
    Arg.(value & opt format Turtle & info ["output-format"; "o"] ~env ~docv:"OUTPUT_FORMAT" ~doc)

  let print_triples format triples =
    match format with
    | Debug ->
      Lwt_fmt.printf "%a@."
        Fmt.(list Rdf.Triple.pp)
        triples
    | Json ->
      Yojson.Basic.pretty_print
        Format.std_formatter
        (triples |> Rdf_json.encode)
      |> Lwt.return
    | Turtle ->
      Lwt_fmt.printf "%s@."
        Rdf.Graph.(empty
                   |> add_seq (List.to_seq triples)
                   |> Rdf_turtle.encode)


  let print_object format fragment_graph =
    Dmc.FragmentGraph.to_triples fragment_graph
    |> List.of_seq
    |> print_triples format

end



(* Common options for the dmc cli. *)

module Options = struct

  open Cmdliner

  type t =
    {override_state_dir: string option;
     output_format: rdf_format;
     input_format: rdf_format}

  let docs = Manpage.s_common_options

  let debug_arg =
    let doc = "Enable debug output" in
    Arg.(value & flag & info ["debug"] ~docs ~doc)

  let override_state_dir_arg =
    let doc = "Override the directory where local state is stored (defaults to \\$XDG_DATA_HOME/dmc or ~/.local/share/dmc)." in
    let env = Arg.env_var "DMC_STATE_DIR" ~doc in
    Arg.(value & opt (some string) ~vopt:None None & info ["state-dir"] ~env ~docv:"STATE_DIR" ~doc)

  let log_level_t =
    let doc = " Set the log level" in
    let env = Arg.env_var "DMC_LOG_LEVEL" ~doc in
    Logs_cli.level ~env ()

  let style_renderer_t =
    Fmt_cli.style_renderer ()

  let term =
    Term.(
      (const (fun override_state_dir
               output_format
               input_format
               style_renderer log_level ->
               (* setup tty *)
               Fmt_tty.setup_std_outputs ?style_renderer ();

               (* setup logger  *)
               Logs.set_reporter (Logs_fmt.reporter ());
               Logs.set_level log_level;

               (* return other common options *)
               {override_state_dir;
                output_format;
                input_format}))
      $ override_state_dir_arg
      $ Output.rdf_format_t
      $ Input.rdf_format_t
      $ style_renderer_t
      $ log_level_t)

  (* Some other commonly used aruments *)

  let container_id_t =
    let open Cmdliner in
    let doc = "Container identifier" in
    Term.(pure (fun s -> Dmc.ContainerIdentifier.of_iri_exn @@ Rdf.Iri.of_string s)
          $ Arg.(required & pos 0 (some string) None & info [] ~docv:"CONTAINER_IDENTIFIER" ~doc))
end


(* Some helpers and commands for accesing the DMC store *)

module Store  = struct

  (* Helpers to find the state directory *)

  let get_xdg_state_dir () =
    match Sys.getenv_opt "XDG_DATA_HOME" with
    | Some dir -> dir ^ "/dmc"
    | None -> (Sys.getenv "HOME") ^ "/.local/share/dmc"

  let get_state_dir (options:Options.t) =
    match options.override_state_dir with
    | Some dir -> dir
    | None -> get_xdg_state_dir ()

  (* Helpers for info *)

  let info msg =
    let date = Unix.gettimeofday () |> Float.round |> Int64.of_float in
    Irmin.Info.v ~date  ~author:"dmc cli" msg

  (* Helpers to open and close a store *)

  let open' options =
    let state_dir = get_state_dir options in
    let config = Irmin_pack.config state_dir in
    let* repo = Backend.Repo.v config in
    (* open the master branch *)
    Backend.master repo

  let close store =
    Backend.Repo.close (Backend.repo store)

  (* TODO add a confirm option *)
  let clear options =
    let state_dir = get_state_dir options in
    (* Clear the entire store by setting fresh to true *)
    let config = Irmin_pack.config ~fresh:true state_dir in
    let* repo = Backend.Repo.v config in
    let* store = Backend.master repo in
    Log.app (fun m -> m "Store cleared.");
    close store

  let clear_cmd =
    let open Cmdliner in
    let doc = "Clear the entire store. WARNING: this will irreversibly delete all data in the store." in
    Term.(const Lwt_main.run $ (const clear $ Options.term)),
    Term.info "store_clear" ~doc
end



(* General container commands and replica inspection commands *)
module Replica = struct
  let object_graph options container_id =
    let* store = Store.open' options in
    Dmc.(
      get_tree store
      >>= Objects.to_triples container_id
      >>= Output.print_triples options.output_format)

  let object_graph_cmd =
    let open Cmdliner in
    let doc = "Get the object graph of a container" in
    Term.(const Lwt_main.run $
          (const object_graph $ Options.term $ Options.container_id_t)),
    Term.info "object_graph" ~doc

  let replicas options =
    let* store = Store.open' options in
    let* replicas = Dmc.(get_tree store >>= replicas) in
    Log.app (fun m -> m "%a@."
                Fmt.(list Dmc.ContainerIdentifier.pp)
                replicas);
    Store.close store

  let replicas_cmd =
    let open Cmdliner in
    let doc = "Print a list of local replicas of DMC containers in the store" in
    Term.(const Lwt_main.run $ (const replicas $ Options.term)),
    Term.info "replicas" ~doc

end


module Set_cmd = struct

  let init options =
    let* store = Store.open' options in

    (* Initialize a new set *)
    let* container_id, secret_key, tree =
      Dmc.(get_tree store >>= Set.initialize) in

    (* Get the container state *)
    let* graph = Dmc.(tree |> Set.resolve container_id) in

    (* commit the tree *)
    let info = Store.info "set_init" in
    let* () = Dmc.commit_tree store ~info tree in

    (* output stuff and clean up *)
    Log.app (fun m -> m "Set identifier: %a"
                (Fmt.styled (`Fg `Blue) Dmc.ContainerIdentifier.pp)
                container_id);
    Log.app (fun m -> m "Secret key (do not loose or share!): %a"
                (Fmt.styled (`Fg `Magenta) Dmc.Signify.SecretKey.pp)
                secret_key);
    let* () = Output.print_triples options.output_format
        (Rdf.Graph.to_triples graph |> List.of_seq) in
    Store.close store

  let init_cmd =
    let open Cmdliner in
    let doc = "Initialize a new DMC set and output the set definition." in
    Term.(const Lwt_main.run $ (const init $ Options.term)),
    Term.info "set_init" ~doc

  let secret_key_t =
    let open Cmdliner in
    let doc = "Secret key used to sign the operation" in
    Term.(
      pure (fun s -> Dmc.Signify.SecretKey.of_iri_exn @@ Rdf.Iri.of_string s)
      $ Arg.(required & opt (some string) None & info ["s"; "secret-key"] ~docv:"SECRET_KEY" ~doc))

  let reference_t =
    let open Cmdliner in
    let doc = "Reference to add" in
    Term.(
      pure (fun s -> Dmc.Eris.of_iri_exn @@ Rdf.Iri.of_string s)
      $ Arg.(required & pos ~rev:true 0 (some string) None & info [] ~docv:"REFERENCE" ~doc))

  let add_ref options container_id secret_key reference =
    let* store = Store.open' options in
    let* operation, tree = Dmc.(
        get_tree store
        >>= Set.add_ref container_id ~secret_key ~element:reference) in
    let info = Store.info "set_add_ref" in
    let* () = Dmc.commit_tree store ~info tree in
    Log.app (fun m ->
        m "Set: %a@.Element URN: %a@.Operation URN: %a@.@.Element added to set with a dmc:Add operation.@."
          Dmc.ContainerIdentifier.pp container_id
          Dmc.Eris.pp reference
          Dmc.Eris.pp operation);
    Store.close store

  let add_ref_cmd =
    let open Cmdliner in
    let doc = "Add a reference to an element to the set." in
    Term.(const Lwt_main.run $ (const add_ref
                                $ Options.term
                                $ Options.container_id_t
                                $ secret_key_t
                                $ reference_t)),
    Term.info "set_add_ref" ~doc


  let add_binary options container_id secret_key filename_opt =
    let* store = Store.open' options in
    let* content = Input.read_raw filename_opt in

    (* Add the reference to the raw data to the set *)
    let* content_urn, operation_urn, tree =
      Dmc.(
        get_tree store
        >>= Set.add_binary container_id ~secret_key ~content)
    in

    (* Commit the tree *)
    let info = Store.info "set_add_raw" in
    let* () = Dmc.commit_tree store ~info tree in

    Log.app (fun m ->
        m "Set: %a@.Content URN: %a@.Operation URN: %a@.@.Content added to set with a dmc:Add operation.@."
          Dmc.ContainerIdentifier.pp container_id
          Dmc.Eris.pp content_urn
          Dmc.Eris.pp operation_urn);
    Store.close store

  let add_binary_cmd =
    let open Cmdliner in
    let doc = "Add binary content to the set." in
    Term.(const Lwt_main.run $ (const add_binary
                                $ Options.term
                                $ Options.container_id_t
                                $ secret_key_t
                                $ Input.filename_t)),
    Term.info "set_add_binary" ~doc

  let operation_to_remove_t =
    let open Cmdliner in
    let doc = "URN of operation to remove" in
    Term.(
      pure (fun s -> Dmc.Eris.of_iri_exn @@ Rdf.Iri.of_string s)
      $ Arg.(required & pos ~rev:true 0 (some string) None & info [] ~docv:"OPERATION" ~doc))

  let remove options container_id secret_key operation_to_remove =
    let* store = Store.open' options in
    let* operation, tree = Dmc.(
        get_tree store
        >>= Set.remove container_id ~secret_key ~operation:operation_to_remove) in
    let info = Store.info "set_remove" in
    let* () = Dmc.commit_tree store ~info tree in
    let* () = Lwt_fmt.printf
        "Set: %a\nOperation to be removed: %a\nRemove operation: %a\n\nRemove operation added to container.\n"
        Dmc.ContainerIdentifier.pp container_id
        Dmc.Eris.pp operation_to_remove
        Dmc.Eris.pp operation in
    Store.close store

  let remove_cmd =
    let open Cmdliner in
    let doc = "Remove an operation from the set." in
    Term.(const Lwt_main.run $ (const remove
                                $ Options.term
                                $ Options.container_id_t
                                $ secret_key_t
                                $ operation_to_remove_t)),
    Term.info "set_remove" ~doc

  let members options container_id =
    let* store = Store.open' options in
    let* members = Dmc.(
        get_tree store
        >>= Set.members container_id
      ) in
    Logs.app (fun m -> m "%a" Fmt.(list Dmc.Eris.pp) members);
    Store.close store

  let members_cmd =
    let open Cmdliner in
    let doc = "Prints a list of set members." in
    Term.(const Lwt_main.run $ (const members
                                $ Options.term
                                $ Options.container_id_t)),
    Term.info "set_members" ~doc
end

module AuthorizedKeys_cmd = struct

  let list options container_id =
    let* keys = Dmc.(
        Store.open' options
        >>= get_tree
        >>= AuthorizedKeys.to_list container_id)
    in
    Log.app (fun m -> m "%a" Fmt.(list Dmc.Signify.PublicKey.pp) keys);
    Lwt.return_unit

  let list_cmd =
    let open Cmdliner in
    let doc = "Print list of authorized keys for a container." in
    Term.(const Lwt_main.run $ (const list
                                $ Options.term
                                $ Options.container_id_t)),
    Term.info "authorized_keys_list" ~doc

  let authorize_new options container_id root_secret_key =
    let* store = Store.open' options in
    let secret_key = Dmc.Signify.generate_secret_key () in
    let public_key = Dmc.Signify.public_key secret_key in
    let* _operation, tree = Dmc.(
        get_tree store
        >>= AuthorizedKeys.authorize container_id ~root_secret_key public_key)
    in
    let info = Store.info "authorized_keys_new" in
    let* () = Dmc.commit_tree store ~info tree in
    Log.app (fun m -> m "Public key %a authorized for container %a@."
                Dmc.Signify.PublicKey.pp public_key
                Dmc.ContainerIdentifier.pp container_id);
    Log.app (fun m -> m "Secret key (do not loose or share!): %a"
                (Fmt.styled (`Fg `Magenta) Dmc.Signify.SecretKey.pp)
                secret_key);
    Store.close store


  let authorize_new_cmd =
    let open Cmdliner in
    let doc = "Authorize a newly generated key for a container." in
    Term.(const Lwt_main.run $ (const authorize_new
                                $ Options.term
                                $ Options.container_id_t
                                $ Set_cmd.secret_key_t)),
    Term.info "authorize_new" ~doc

end

module Resolve_cmd = struct

  let resolve options container_id =
    let* store = Store.open' options in
    let* graph = Dmc.(
        get_tree store
        >>= Set.resolve container_id ~deep:true)
    in
    Output.print_triples options.output_format
      (Rdf.Graph.to_triples graph |> List.of_seq)

  let resolve_cmd =
    let open Cmdliner in
    let doc = "Resolve a URN" in
    Term.(const Lwt_main.run $ (const resolve
                                $ Options.term
                                $ Options.container_id_t)),
    Term.info "resolve" ~doc
end

module Repl = struct

  type command =
    | Quit

  let command_parser =
    let open Angstrom in
    string ",quit" *> return Quit

  type input =
    | Command of command
    | Clause of Dmc.Datalog.Clause.t
    | Query of Dmc.Datalog.Atom.t

  let parser =
    let open Angstrom in
    choice ~failure_msg:"not a command, clause or query"
      [
        command_parser >>| (fun c -> Command c);
        Dmc.Datalog.Parser.clause >>| (fun c -> Clause c);
        Dmc.Datalog.Parser.query >>| (fun q -> Query q);
      ]

  let parse line =
    Angstrom.(parse_string ~consume:Consume.All parser line)


  let repl options container_id =
    let* store = Store.open' options in
    let* db = Dmc.(
        get_tree store
        >>= Objects.to_datalog_db container_id
      )
    in
    let rec loop () =
      let* input =
        Lwt_io.printf "DMC> " >>= fun () ->
        Lwt_io.read_line Lwt_io.stdin >|= parse in

      match input with
      | Ok (Command Quit) ->
        Log.app (fun m -> m "Bye!")
        |> Lwt.return

      | Ok (Clause c) ->
        Dmc.Datalog.DB.add_clause db c
        |> loop

      | Ok (Query q) ->
        let results = Dmc.Datalog.DB.prove db q in
        Log.app (fun m -> m "%a@."
                    Fmt.(list Dmc.Datalog.Atom.pp)
                    results)
        |> loop

      | Error msg ->
        Log.app (fun m -> m "Invalid input: %s@." msg)
        |> loop
    in

    Log.app (fun m -> m "Starting DMC Datalog REPL.@.Enter ,quit to exit.@.")
    |>  loop

  let repl_cmd =
    let open Cmdliner in
    let doc = "Start a Datalog REPL" in
    Term.(const Lwt_main.run $ (const repl $ Options.term $ Options.container_id_t)),
    Term.info "repl" ~doc

end


module Eris = struct
  (* let sum options filename_opt = *)
  (*   let* store = Store.open' options in *)
  (*   let* input = Input.read_raw filename_opt in *)

  (*   (\* Add the raw data to a dummy container *\) *)
  (*   let* urn, _tree = *)
  (*     Dmc.( *)
  (*       get_tree store *)
  (*       (\* This trick with the dummy does not work anymore! *\) *)
  (*       >>= Objects.add_binary (Rdf.Iri.of_string "urn:dummy") input *)
  (*     ) *)
  (*   in *)
  (*   (\* Do NOT commit as we are just interested in the URN *\) *)

  (*   (\* Ouput the URN *\) *)
  (*   Log.app (fun m -> m "%a" Rdf.Iri.pp urn) *)
  (*   |> Lwt.return *)

  (* let sum_cmd = *)
  (*   let open Cmdliner in *)
  (*   let doc = "Compute the ERIS read capability" in *)
  (*   Term.(const Lwt_main.run $ (const sum $ Options.term $ Input.filename_t)), *)
  (*   Term.info "eris_sum" ~doc *)

  let urn_t =
    let open Cmdliner in
    let doc = "ERIS URN" in
    Term.(
      pure (fun s -> Dmc.Eris.of_iri_exn @@ Rdf.Iri.of_string s)
      $ Arg.(required & pos 0 (some string) None & info [] ~docv:"ERIS_URN" ~doc))

  let get options urn =
    let* store = Store.open' options in
    let* tree = Dmc.get_tree store in
    let* fragment_graph = Dmc.Eris.get_object urn tree in
    match fragment_graph with
    | Ok fg ->
      Output.print_object options.output_format fg
    | _ ->
      Dmc.Eris.get urn tree
      >>= Lwt_io.printl

  let get_cmd =
    let open Cmdliner in
    let doc = "Get ERIS encoded content from the store. If content can not be decoded as RDF it will be written directly to standard output." in
    Term.(const Lwt_main.run $ (const get $ Options.term $ urn_t)),
    Term.info "eris_get" ~doc

end

module Cbor_cmd = struct
  module Dmc_cbor = Dmc_cbor.Make(Dmc)

  type encoding =
    | Raw
    | Base64
    | Base32

  let encoding_t =
    let open Cmdliner in
    let doc = "Encoding of CBOR (valid values are raw, base32 and base64)." in
    let encoding = Arg.enum ["raw", Raw; "base32", Base32; "base64", Base64] in
    Arg.(value & opt encoding Base32 & info ["encoding"; "e"] ~docv:"CBOR_ENCODING" ~doc)

  let dump options objects blocks encoding container_id =
    let* store = Store.open' options in
    let* cbor = Dmc.get_tree store
      >>= Dmc_cbor.to_binary container_id ~objects ~blocks
    in
    match encoding with
    | Raw ->
      cbor
      |> Lwt_io.print
      >>= Lwt_io.flush_all
    | Base64 ->
      cbor
      |> Base64.encode_string
      |> Lwt_io.printl
      >>= Lwt_io.flush_all
    | Base32 ->
      cbor
      |> Base32.encode_string
      |> Lwt_io.printl
      >>= Lwt_io.flush_all

  let dump_cmd =
    let open Cmdliner in
    let doc = "Get CBOR serialization of replica state" in
    let objects_t =
      Arg.(value & vflag true [
          false, Arg.info ["no-objects"] ~doc:"Do not include replica objects in CBOR dump"]) in
    let blocks_t =
      Arg.(value & vflag true [
          false, Arg.info ["no-blocks"] ~doc:"Do not include replica blocks in CBOR dump"]) in
    Term.(const Lwt_main.run $ (const dump
                                $ Options.term
                                $ objects_t
                                $ blocks_t
                                $ encoding_t
                                $ Options.container_id_t)),
    Term.info "cbor_dump" ~doc

  let load options filename encoding =
    (* read input *)
    let* input_raw = Input.read_raw filename in
    (* decode encoding *)
    let input =
      match encoding with
      | Raw -> input_raw
      | Base64 -> Base64.decode_exn @@ String.trim input_raw
      | Base32 -> Base32.decode_exn @@ String.trim input_raw
    in
    (* open store and load the cbor *)
    let* store = Store.open' options in
    let* tree = Dmc.get_tree store
      >>= Dmc_cbor.load input
    in
    let info = Store.info "cbor_load" in
    let* () = Dmc.commit_tree store ~info tree in
    Store.close store

  let load_cmd =
    let open Cmdliner in
    let doc = "Load replica state from CBOR" in
    Term.(const Lwt_main.run $ (const load
                                $ Options.term
                                $ Input.filename_t
                                $ encoding_t)),
    Term.info "cbor_load" ~doc

end


module Main = struct
  let help_secs =
    let open Cmdliner in
    [
      `S Manpage.s_common_options;
      `P "These options are common to all commands.";
      `S "MORE HELP";
      `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";

      `S Manpage.s_authors;
      `P "DREAM (https://dream.public.cat/)";

      `S "LICENSE";
      `P "AGPL-3.0-or-later";
      `P "The source code is available at https://gitlab.com/public.dream/dromedar/ocaml-dmc"
    ]

  let default_cmd =
    let open Cmdliner in
    let doc = "Distributed Mutable Containers (http://purl.org/dmc)" in
    let sdocs = Manpage.s_common_options in
    let exits = Term.default_exits in
    let man = help_secs in
    Term.(ret (const (fun _ -> `Help (`Pager, None)) $ Options.term)),
    Term.info "dmc" ~version:"0.1.0-alpha" ~doc ~sdocs ~exits ~man

  let cmds = [
    (* Store maintenance commands *)
    Store.clear_cmd;

    (* Set commands *)
    Set_cmd.init_cmd;
    Set_cmd.add_ref_cmd;
    Set_cmd.add_binary_cmd;
    Set_cmd.remove_cmd;
    Set_cmd.members_cmd;

    (* Authorized keys *)
    AuthorizedKeys_cmd.list_cmd;
    AuthorizedKeys_cmd.authorize_new_cmd;

    (* Resolve *)
    Resolve_cmd.resolve_cmd;

    (* REPL *)
    Repl.repl_cmd ;

    (* CBOR *)
    Cbor_cmd.dump_cmd;
    Cbor_cmd.load_cmd;

    (* Store introspection commands *)
    Replica.replicas_cmd;
    Replica.object_graph_cmd;

    (* ERIS commands *)
    Eris.get_cmd;
  ]
end

let () =
  let open Cmdliner in
  Term.(exit @@ eval_choice Main.default_cmd Main.cmds)
