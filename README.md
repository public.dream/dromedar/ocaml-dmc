# Distributed Mutable Containers

This repository contains the reference implementation of [Distributed Mutable Containers (DMC)](https://gitlab.com/public.dream/dromedar/dmc) in OCaml.

***WARNING: `ocaml-dmc` is alpha quality software and backwards compatibility is not guaranteed. See the section "Future Work" for more information.***

# Overview

`ocaml-dmc` consists of an OCaml library (`dmc`) as well as a Unix command line interface tool (`dmc_cli`).

## Library

The OCaml library consists of following packages:

- `dmc`: The core `Dmc` module.
  - `dmc.crypto`: Signatures for cryptographic primitives.
  - `dmc.cbor`: Cbor serialization of a DMC replica.
- `dmc_crypto.unix`: An implementation of the `dmc.crypto` signatures for Unix (using Monocypher and Cryptokit)

See the [online API documentation](https://inqlab.net/projects/ocaml-dmc/index.html) or build the documentation locally with `dune build @doc`.

### Usage 

``` ocaml
open Lwt.Infix
open Lwt.Syntax

(* Irmin in-memory store with string content (and keys) *)
module Store = Irmin_mem.KV(Irmin.Contents.String)

(* Instantiate Dmc using the Irmin store and cryptographic primitives *)
module Dmc = Dmc.Make(Dmc_crypto_unix)(Store)

(* Irmin database configuration *)
let config = Irmin_mem.config ()

let main = 
  let* store = 
    (* Open the repo **)
    Store.Repo.v config >>=
    (* Load the master branch *)
    Store.master
  in
  
  (* Initialize a new DMC set. *)
  let* conatiner_id, secret_key, tree =
    Dmc.(get_tree store
         >>= Set.initialize) in
         
  (* Using the secret key, we can add some content to the set *)
  let content = "Hello DMC!" in
  let* content_urn, operation_urn, tree =
    Dmc.Set.add_binary container_id ~secret_key ~content
  in
  
  (* We must persist changes manually *)
  let* () =
    Dmc.commit_tree store tree
  in
  
  (* We can query for all members of the set *)
  let* members = 
    Dmc.(get_tree store
        >>= Set.members container_id)
  in
  
  (* We can add some more content *)
  let content2 = "Hello again DMC!" in
  let* content2_urn, operation2_urn, tree =
    Dmc.(get_tree
         >>= Set.add_binary container_id ~secret_key ~content:content2)
  in
  
  (* And also remove the initial message *)
  let* remove_operation_urn, tree =
    Dmc.(get_tree store
         (* Note how we need to reference the operation that added the element 
            we want to remove and not the element itself. This is how an 
            Observed-Remove set works. See the specification document for
            more information. *)
         >>= Set.remove container_id ~secret_key ~operation:operation_urn)
  in

  (* Again we commit the tree to persist changes *)
  let* () =
    Dmc.commit_tree store tree
  in
  
(* Run the program *)
let () = Lwt_main.run main
```

## CLI

A Unix command line interface tool is provided in the package `dmc_cli`. This CLI is intended to demonstrate capabilities of DMC.

See the section "Hacking" on how to run the CLI from a Git checkout. You may need to use the command `dune exec src/cli/dmc_cli.exe` instead of `dmc` to re-run the examples.

See also `dmc --help` for more commands and information.

### Initializing a set

``` sh
$ dmc set_init
Set identifier: dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4
Secret key (do not loose or share!): urn:ed25519:sk:2SRPXS3TI6ZJKBYI7CF4GUZ3MFNT4FD57DJP64PX26P74ATMXLSA


<urn:erisx2:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4>
 <http://purl.org/dmc/ns#rootPublicKey> <urn:ed25519:pk:P7EAWHGZA32BETGID3OXHN2JVJ5GRJLAQGPHRF55I7VMGBC7DYDQ> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/dmc/ns#SetDefinition> .
```

This will initialize a new Set where elements can be added.

### Add an element to the set

``` sh
$ dmc set_add_binary \\
    dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4 \\
    --secret-key urn:ed25519:sk:2SRPXS3TI6ZJKBYI7CF4GUZ3MFNT4FD57DJP64PX26P74ATMXLSA

Reading from standard input (ctrl-d to terminate).
Hi! This is my first step with DMC.
Set: dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4
Element: urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU
Operation: urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU

Element added to set with a dmc:Add operation.
```

This adds a short message which is read from standard input.

### Members of set

``` sh
$ dmc set_members \\
    dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4

urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU 
```

This will print a list of elements in the set (the members of the set).

### Getting the data

``` sh
$ dmc eris_get urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU
Hi! This is my first step with DMC.
```

The `eris_get` command can be used to access ERIS encoded content.

Note that the operation that added our short note is also ERIS encoded content and can also be accessed as such:

``` sh
$ dmc eris_get urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU

<urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU>
 <http://purl.org/dmc/ns#container> <dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/dmc/ns#Add> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> <urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU> .

```

This prints out the `dmc:Add` operation in Turtle (an early stage of Turtle).

The CLI can also output RDF content in other formats, for example as RDF/JSON:

``` sh
$ dmc eris_get -o json urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU
{
  "urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU": {
    "http://purl.org/dmc/ns#container": [
      {
        "type": "uri",
        "value":
          "dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4"
      }
    ],
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": [
      { "type": "uri", "value": "http://purl.org/dmc/ns#Add" }
    ],
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#value": [
      {
        "type": "uri",
        "value":
          "urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU"
      }
    ]
  }
```


## Removing an element

To remove an element from the set we need to reference the operation that added the element (this is how Observed-Remove sets work; see the specification for more information on how this works):

``` sh
$ dmc set_remove dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4 --secret-key urn:ed25519:sk:2SRPXS3TI6ZJKBYI7CF4GUZ3MFNT4FD57DJP64PX26P74ATMXLSA urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU
Set: dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4
Operation to be removed: urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU
Remove operation: urn:erisx2:BIAJ6KXKLVNPKUS32MP3Q3THZ7PAL36TFYN7IGSHJMKMJMLUM5Y56XTZ6A453AEY7FXUUSDZS6LTJVLME2NYV6MDSGL46TC32ABXBW4VQQ

Remove operation added to container.
```

If we now query the members of the set we see that our little note has been removed (nothing will be output):

``` sh
$ set_members dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4
```


## The Object Graph

The local state of a DMC container is an RDF Graph - the object graph. This object graph can be inspected:

```
$ dmc object_graph dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4


<urn:erisx2:BIAFMLKQ43CY3SSSRQBGLBIDUW6KZ6UAXGYXR5YYJOITPGSIQUYXY3PGPXLJGL6U4MF4MMCNJLQ2XZA2NDCICKGJOJUGJDNOOR6XAHKEMY>
 <http://purl.org/signify/ns#message> <urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/signify/ns#Signature> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>  "UYrx1eeO9QO1WubMr0yVTrMSnS77FwePFNY6j68LPxBonZmcFSAb5dXrrDt8QEzHRfZu1Q/6jYdkei5MvZBDAA=="^^
                                                             <http://www.w3.org/2001/XMLSchema#base64Binary>  .

<urn:erisx2:BIAJ6KXKLVNPKUS32MP3Q3THZ7PAL36TFYN7IGSHJMKMJMLUM5Y56XTZ6A453AEY7FXUUSDZS6LTJVLME2NYV6MDSGL46TC32ABXBW4VQQ>
 <http://purl.org/dmc/ns#container> <dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4> ;
  <http://purl.org/dmc/ns#operation> <urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/dmc/ns#Remove> .

<urn:erisx2:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4>
 <http://purl.org/dmc/ns#rootPublicKey> <urn:ed25519:pk:P7EAWHGZA32BETGID3OXHN2JVJ5GRJLAQGPHRF55I7VMGBC7DYDQ> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/dmc/ns#SetDefinition> .

<urn:erisx2:BIAJXMXFLYUWXOBZY2NHKZDOZYDIPW5XNZWFCD3LWBRPQBJ3VNVN6WLOKQGPXOIPYN7RSA6T5PVUBT77UIDWYNVOV3JHR5HZDRMELM4PKY>
 <http://purl.org/signify/ns#message> <urn:erisx2:BIAJ6KXKLVNPKUS32MP3Q3THZ7PAL36TFYN7IGSHJMKMJMLUM5Y56XTZ6A453AEY7FXUUSDZS6LTJVLME2NYV6MDSGL46TC32ABXBW4VQQ> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/signify/ns#Signature> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#value>  "WTGccXzXxYVNu/CNgXm85lAJJekA8O0jyRZQ+NZEEvxlq3o37ABhUDiZRiz/SXTtYgHas9JyyOCfD8l+nE03CA=="^^
                                                             <http://www.w3.org/2001/XMLSchema#base64Binary>  .

<urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU>
 <http://purl.org/dmc/ns#container> <dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/dmc/ns#Add> ;
  <http://www.w3.org/1999/02/22-rdf-syntax-ns#value> <urn:erisx2:BIAI2NAHAXOSO7EYRDVFVWOJSTVB4QFAPPHNGF35OQ6EUUSRFN4W5B6CIIADWDUMV2ADTEGN3KZMPIMZ2VABRGXM7A3GXKCXSHNJQGBDZU> .

```

We see that now there are two operations: A `dmc:Add` and a `dmc:Remove`. Our little message was not deleted but just marked as removed. It can be removed definitely by a garbage collection process. This still needs to be implemented. Stay tuned!

## Datalog REPL

The CLI also has a built-in Datalog REPL with which you can go spelunking into the nitty-gritty internals of DMC. For example you can inspect the authorized operations:

``` sh
repl dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4
Starting DMC Datalog REPL.
Enter ,quit to exit.

DMC> authorizedOperation(<dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4>,?1)?
authorizedOperation(<iri dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4>,
                    <iri urn:erisx2:BIAJ6KXKLVNPKUS32MP3Q3THZ7PAL36TFYN7IGSHJMKMJMLUM5Y56XTZ6A453AEY7FXUUSDZS6LTJVLME2NYV6MDSGL46TC32ABXBW4VQQ>)
authorizedOperation(<iri dmc:BIAJ7POZLK5ES6CHZBFRR4L6UWGV2A5AWVA4DPULHGB3ERG3735RUT4PC7NSLGX3MQZ3DGFLNQQR7AG36XCX7KPTNBTFZWCGXYUYABBEI4>,
                    <iri urn:erisx2:BIALZOIKZGPPGK6OSSGRTMNMFU5OLUDJLAQ2DRFH6BXIFJ4SQRSLUXOCABZGIXXNQ7ELVOXUUDAIHWMON63G4ZBDILKZRHDE2AHDA6SYJU>)

```

This will print out all operations that are considered when computing the state of a container.

You can also use the REPL to compute a transitive closure:

``` sh
DMC> edge("a","b").
DMC> edge("b","c").
DMC> path(?1,?2) :- edge(?1,?2).
DMC> path(?1,?2) :- path(?1,?3), edge(?3,?2).
DMC> path(?1,?2)?
path(<literal a^^http://www.w3.org/2001/XMLSchema#string>, <literal b^^http://www.w3.org/2001/XMLSchema#string>)
path(<literal b^^http://www.w3.org/2001/XMLSchema#string>, <literal c^^http://www.w3.org/2001/XMLSchema#string>)
path(<literal a^^http://www.w3.org/2001/XMLSchema#string>, <literal c^^http://www.w3.org/2001/XMLSchema#string>)
```

The REPL gives access to a complete Datalog inference engine. This allows interesting queries and inference on data in a DMC container.


# Hacking

The easiest way to get a development environment for working on ocaml-dmc is to use [Guix](https://guix.gnu.org/):

``` sh
guix environment -l guix.scm
```

This will create an environment where you build everything with `dune`:

``` sh
dune build
```

Tests can be run with `dune test` and you can build the API documentation with `dune build @doc`.

# Future work

## Specification
 
 Currently `ocaml-dmc`` is not suitable for real-world usage as it relies on specifications that have not yet been finalized. Changes to these specifications might result in incompatibility between versions of ocaml-dmc that implement different versions of the specifications.

### ERIS

`ocaml-dmc` uses [ERIS](http://purl.org/eris) (as specified by [DMC](http://purl.org)). However ERIS still needs to be finalized with a version 1.0 release.

### Content-addressable RDF

[DMC](http://purl.org) uses the canonical serialization as specified in [Content-addressable RDF](http://purl.org/ca-rdf). The C-Sexp based serialization as defined in version 0.1 of the Content-addressable RDF document is obsolte and is scheduled to be replaced with a CBOR based [HDT](rdfhdt.org) based serialization. More work is required to figure out and specify this serialization.

### RDF Signify

RDF Signify is fairly simple and straightforward. No major revisions are anticipated to the core of the vocabulary. However, RDF Signify uses Crypto URIs which should be discussed with a wider community before finalizing (similar efforts [CryptoURI](https://github.com/cryptouri) exist).

## Implementation and API

The implementation and API is bound to change and improve. Backwards compatibility of the `dmc` library interface as well as the on-disk persistence format of `dmc_cli` is not guaranteed.

### Datalog

Currently `ocaml-dmc` uses an existing Datalog implementation ([c-cube/datalog](https://github.com/c-cube/datalog]). The implementation works fine and has many advanced features built-in that are used by `ocaml-dmc` (support for interpreted predicates, negation). However the implementation can be improved in following ways:

- Use Irmin storage for data: Currently all data is first loaded into memory before being evaluated in Datalog.
- Use specialized (on-disk) indexes to improve performance. This could also enable user-specified indexes (e.g. full-text search or geospatial).
- c-cube/datalog supports nested terms (e.g. `p(q(a))` is a valid term). This is not needed (and maybe not wanted) in `ocaml-dmc`. Currently an awkward wrapper hides this from users of `ocaml-dmc`.
- Variables can only be integers. Allowing arbitrary symbols as variables would make Datalog clauses much more readable.

### Irmin Watches

[Irmin Watches](https://mirage.github.io/irmin/irmin/Irmin/module-type-S/index.html#watches) allow changes to the Irmin store to be observed. This can be used to implement transport layers that propagate changes over networks.

## Transports

Various transport protocols may be implemented. See also the [DMC](http://purl.org/dmc) specification for further information.

### HTTP

A transport over HTTP would allow Web/JavaScript clients to synchronize with a DMC replica.

### XMPP

XMPP can be used as a general-purpose pub-sub system. Maybe it could be used to syncrhonize DMC replicas?

There are OCaml libraries for working with XMPP (https://github.com/hannesm/xmpp) and even libraries that might allow end-to-end encryption (https://github.com/hannesm/ocaml-otr).

### MQTT

MQTT is a light-weight pub-sub protocol. There are MQTT libraries for OCaml (https://github.com/odis-labs/ocaml-mqtt).

### Upscyle

[Upsycle](https://public.dream.gitlab.io/upsycle/upsycle/design.html) is a peer-to-peer pub-sub system developed as part of the [P2PCollab](https://p2pcollab.net/) and [DREAM](https://dream.public.cat/). It may be used as a transport to synchronize DMC replicas.

## Interfaces

Interfaces are ways of accessing DMC containers as opposed to transports that are used to synchronize replicas. The CLI is an interface to DMC containers.

### HTTP

A simple HTTP interface for accessing and modifying DMC containers.

### ActivityPub

An ActivityPub interface to a DMC container that allows following a DMC container's changes.

### 9P

A 9P interface that would allow mounting a DMC container as a Unix file-system.

There is an OCaml 9P library (https://github.com/mirage/ocaml-9p).

# Contact

`ocaml-dmc` is developed as part of the [DREAM](https://dream.public.cat/) project.

[dream [at] public.cat](mailto:dream@public.cat)

# License

[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt)
